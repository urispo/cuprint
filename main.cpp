#include <limits.h>
#include <errno.h>

#include "cu_print.h"


extern "C"
{
    std::string
    recodeText( const char* text, const char* srcEnc, const char* dstEnc )
    {
        errno = 0;

        iconv_t hconv = iconv_open( dstEnc, srcEnc );

        if( ((iconv_t)(-1)) == hconv ) {
            return "";
        }

        size_t      ibufsz = strlen( text );
        char* const src = (char*) malloc( ibufsz );
        size_t      obufsz = MB_LEN_MAX * ibufsz;
        char* const dst = (char*) malloc( obufsz );
        char*       ibuf = (char*) memcpy( src, text, ibufsz );
        char*       obuf = dst;
        std::string Result;


        errno = 0;
        iconv( hconv, &ibuf, &ibufsz, &obuf, &obufsz );
        Result = std::string( dst, obuf - dst );

        free( src );
        free( dst );
        iconv_close( hconv );


        return Result;
    }
    //--------------------------------------------------------------------

    int
    bindPrinter( void** phPrinter, const char* printerModel, const char* printerPort )
    {
        char*   vendor = NULL;
        char*   model = NULL;
//        char*   protocol = NULL;
        char    buffer[256];


        strncpy( buffer, printerModel, sizeof(buffer)-1 );
        buffer[ sizeof(buffer)-1 ] = '\0';

        char* delim = strchr( buffer, '/' );


        if( NULL != delim ) {
            *delim = '\0';
            model = delim + 1;
        }
        vendor = buffer;


        if( NULL != phPrinter ) {
            if( 0 == strcmp(vendor, "OKI") ) {
                *phPrinter = new TCU_OkiThermal( printerPort, model );
            }
            else
            if( 0 == strcmp(vendor, "DATECS") ) {
                *phPrinter = new TCU_DatecsBT( printerPort, model );
            }
            else
            if( 0 == strcmp(vendor, "PSVirtual") ) {
                *phPrinter = new TCU_PsVirtual( printerPort, model );
            }
            else
            if( 0 == strcmp(vendor, "MITSU") ) {
                *phPrinter = new TCU_MitsuThermal( printerPort, model );
            }
            if( 0 == strcmp(vendor, "NEXGO") ) {
                *phPrinter = new TCU_NexgoN86( printerPort, model );
            }
            else {
                *phPrinter = NULL;

                return CU_PRINT_ERR__INVALID_PROTOCOL;
            }

            TCU_PrinterHandle   Printer = (TCU_PrinterHandle) *phPrinter;
            int result = Printer->getErrorCode();


            if( CU_PRINT_ERR__NONE != result ) {
                delete ( Printer );
                *phPrinter = NULL;
            }

            return result;
        }
        else return CU_PRINT_ERR__INVALID_FUNC_CALL;
    }
    //--------------------------------------------------------------------

    int
    freePrinter( void** phPrinter )
    {
        if( NULL == phPrinter ) {
            return CU_PRINT_ERR__INVALID_FUNC_CALL;
        }

        TCU_PrinterHandle   Printer = (TCU_PrinterHandle) *phPrinter;


        if( NULL != Printer ) {
            delete Printer;
            *phPrinter = NULL;

            return CU_PRINT_ERR__NONE;
        }
        else return CU_PRINT_ERR__INVALID_HANDLE;
    }
    //--------------------------------------------------------------------

    int
    initPrinter( void* hPrinter, json_object* config )
    {
        if( NULL != hPrinter ) {
            TCU_PrinterHandle   Printer = (TCU_PrinterHandle) hPrinter;


            Printer->init( config );

            return Printer->getErrorCode();
        }
        else return CU_PRINT_ERR__INVALID_HANDLE;
    }
    //--------------------------------------------------------------------

    const char*
    getDriverVersion( void* hPrinter )
    {
        if( NULL != hPrinter ) {
            TCU_PrinterHandle   Printer = (TCU_PrinterHandle) hPrinter;


            return ( Printer->getVersion() ).c_str();
        }
        else return "";
    }
    //--------------------------------------------------------------------

    int
    getDeviceState( void* hPrinter, TCU_PrinterState* State )
    {
        if( NULL != hPrinter ) {
            TCU_PrinterHandle   Printer = (TCU_PrinterHandle) hPrinter;


            Printer->getState( State );

            return Printer->getErrorCode();
        }
        else return CU_PRINT_ERR__INVALID_HANDLE;
    }
    //--------------------------------------------------------------------

    int
    doPrintJob( void* hPrinter, json_object* job )
    {
        if( NULL != hPrinter ) {
            TCU_PrinterHandle   Printer = (TCU_PrinterHandle) hPrinter;


            Printer->doPrintJob( job );

            return Printer->getErrorCode();
        }
        else return CU_PRINT_ERR__INVALID_HANDLE;
    }
    //--------------------------------------------------------------------

    std::string
    getSupportedDevs( void )
    {
        return "OKI/PT330, OKI/PT340; DATECS/DPP250; MITSU/RP809; NEXGO/N86";
    }
    //--------------------------------------------------------------------
    //--------------------------------------------------------------------
    //--------------------------------------------------------------------
}
