#include "cu_print.h"


#define DATECS_TP__VERSION_STRING  "0.1"


///*******************************************************************
///PUBLIC METHODS OF DATECS_BT_PRINTER CLASS
///*******************************************************************

TCU_DatecsBT::TCU_DatecsBT( const char* port, const char* model ) :
TCU_BasePrinter( port )
{
    if( 0 == strcmp("DPP250", model) ) {
        modelId = CU_PRINT_MODEL_ID__DPP250;
    }
    else {
        lastError = CU_PRINT_ERR__INVALID_MODEL;

        return;
    }

    if( ((iconv_t)(-1)) == (hconv = iconv_open("CP1251//TRANSLIT", ifceCS)) ) {
        lastError = CU_PRINT_ERR__CREATE_ENC_HANDLE;
        return;
    }
}
//--------------------------------------------------------------------

TCU_DatecsBT::~TCU_DatecsBT()
{
}
//--------------------------------------------------------------------

bool
TCU_DatecsBT::init( json_object* jConfig )
{
    if( CU_PRINT_MODEL_ID__UNDEF == modelId ) {
        lastError = CU_PRINT_ERR__INVALID_MODEL;

        return false;
    }

    DefaultPortSettings.baudrate = 115200;
    DefaultPortSettings.databits = 8;
    DefaultPortSettings.stopbits = 1;
    DefaultPortSettings.parity   = SP_PARITY_NONE;
    DefaultPortSettings.flowcontrol  = SP_FLOWCONTROL_NONE;

    if( ! configPort(getPortSettings(jConfig)) ) {
        lastError = CU_PRINT_ERR__PORT_CONFIG;

        return false;
    }
    clearCmdBuffer();

    if( ! fillCmdBuffer(2, CH__ESC, '@') ) {
        return false;
    }

    json_object*        jValue = NULL;


    if( json_object_object_get_ex(jConfig, "leftMargin", &jValue) ) {
        int n = json_object_get_int( jValue );


        if( ! fillCmdBuffer(4, CH__GS, 'L', n, (n >> 8)) ) {
            return false;
        }
    }
    if( json_object_object_get_ex(jConfig, "printAreaWidth", &jValue) ) {
        int n = json_object_get_int( jValue );


        if( ! fillCmdBuffer(4, CH__GS, 'W', n, (n >> 8)) ) {
            return false;
        }
    }

/// select character code table: CP1251
    if( ! fillCmdBuffer(3, CH__ESC, 'u', 17) ) {
        return false;
    }
/// user character set (copy)
    if( ! fillCmdBuffer(3, CH__ESC, '&', 0) ) {
        return false;
    }
/// Specify printing mode of text data
    printingModeOfTextData = 0;
    if( ! fillCmdBuffer(3, CH__ESC, '!', printingModeOfTextData) ) {
        return false;
    }

    /// define user-defined characters
    char    data[3][48] = {
            {
                0x00, 0x10,
                0x00, 0x30,
                0x00, 0x70,
                0x00, 0xf0,
                0x01, 0xe0,
                0x03, 0xc0,
                0x07, 0x80,
                0x0f, 0x00,
                0x1e, 0x00,
                0x3f, 0xf0,
                0x7f, 0xf0,
                0xff, 0xf0,
                0xff, 0xf0,
                0x7f, 0xf0,
                0x3f, 0xf0,
                0x1e, 0x00,
                0x0f, 0x00,
                0x07, 0x80,
                0x03, 0xc0,
                0x01, 0xe0,
                0x00, 0xf0,
                0x00, 0x70,
                0x00, 0x30,
                0x00, 0x10
            },
            {
                0x00, 0x00,
                0x00, 0x00,
                0x00, 0x00,
                0x00, 0x00,
                0x00, 0x00,
                0x00, 0x00,
                0x00, 0x00,
                0x00, 0x00,
                0x00, 0x00,
                0xff, 0xf0,
                0xff, 0xf0,
                0xff, 0xf0,
                0xff, 0xf0,
                0xff, 0xf0,
                0xff, 0xf0,
                0x00, 0x00,
                0x00, 0x00,
                0x00, 0x00,
                0x00, 0x00,
                0x00, 0x00,
                0x00, 0x00,
                0x00, 0x00,
                0x00, 0x00,
                0x00, 0x00,
            },
            {
                0x80, 0x00,
                0xc0, 0x00,
                0xe0, 0x00,
                0xf0, 0x00,
                0x78, 0x00,
                0x3c, 0x00,
                0x1e, 0x00,
                0x0f, 0x00,
                0x07, 0x80,
                0xff, 0xc0,
                0xff, 0xe0,
                0xff, 0xf0,
                0xff, 0xf0,
                0xff, 0xe0,
                0xff, 0xc0,
                0x07, 0x80,
                0x0f, 0x00,
                0x1e, 0x00,
                0x3c, 0x00,
                0x78, 0x00,
                0xf0, 0x00,
                0xe0, 0x00,
                0xc0, 0x00,
                0x80, 0x00
            }
    };


    if( ! fillCmdBuffer(5, CH__ESC, '&', 2, '<', '>') ) {
        return false;
    }
    for( int i = 0; i < 3; i++ ) {
        if( ! fillCmdBuffer(48, data[i]) ) {
            return false;
        }
    }

    char    diacritics[10][48] = {
        { // Ć
            0x03,	 0x80,
            0x07,	 0x00,
            0x0C,	 0x00,
            0x00,	 0x00,
            0x1F,	 0x80,
            0x3F,	 0xC0,
            0x30,	 0xE0,
            0x60,	 0x60,
            0x60,	 0x00,
            0x60,	 0x00,
            0x60,	 0x00,
            0x60,	 0x00,
            0x60,	 0x00,
            0x60,	 0x00,
            0x60,	 0x00,
            0x60,	 0x00,
            0x60,	 0x00,
            0x60,	 0x60,
            0x30,	 0xE0,
            0x3F,	 0xC0,
            0x1F,	 0x80,
            0x00,	 0x00,
            0x00,	 0x00,
            0x00,	 0x00
        },
        {   // Č
            0x19,	 0x80,
            0x0F,	 0x00,
            0x06,	 0x00,
            0x00,	 0x00,
            0x1F,	 0x80,
            0x3F,	 0xC0,
            0x30,	 0xE0,
            0x60,	 0x60,
            0x60,	 0x00,
            0x60,	 0x00,
            0x60,	 0x00,
            0x60,	 0x00,
            0x60,	 0x00,
            0x60,	 0x00,
            0x60,	 0x00,
            0x60,	 0x00,
            0x60,	 0x00,
            0x60,	 0x60,
            0x30,	 0xE0,
            0x3F,	 0xC0,
            0x1F,	 0x80,
            0x00,	 0x00,
            0x00,	 0x00,
            0x00,	 0x00
        },
        { // Ł
            0x00,	 0x00,
            0x00,	 0x00,
            0x00,	 0x00,
            0x00,	 0x00,
            0x30,	 0x00,
            0x30,	 0x00,
            0x32,	 0x00,
            0x36,	 0x00,
            0x3C,	 0x00,
            0x38,	 0x00,
            0x30,	 0x00,
            0x70,	 0x00,
            0xF0,	 0x00,
            0xB0,	 0x00,
            0x30,	 0x00,
            0x30,	 0x00,
            0x30,	 0x00,
            0x30,	 0x00,
            0x30,	 0x00,
            0x3F,	 0xE0,
            0x3F,	 0xE0,
            0x00,	 0x00,
            0x00,	 0x00,
            0x00,	 0x00
        },
        { // Ĺ
            0x03,	 0x80,
            0x07,	 0x00,
            0x0C,	 0x00,
            0x00,	 0x00,
            0x30,	 0x00,
            0x30,	 0x00,
            0x30,	 0x00,
            0x30,	 0x00,
            0x30,	 0x00,
            0x30,	 0x00,
            0x30,	 0x00,
            0x30,	 0x00,
            0x30,	 0x00,
            0x30,	 0x00,
            0x30,	 0x00,
            0x30,	 0x00,
            0x30,	 0x00,
            0x30,	 0x00,
            0x30,	 0x00,
            0x3F,	 0xE0,
            0x3F,	 0xE0,
            0x00,	 0x00,
            0x00,	 0x00,
            0x00,	 0x00
        },
        { // Ń
            0x03,	 0x80,
            0x07,	 0x00,
            0x0C,	 0x00,
            0x00,	 0x00,
            0x60,	 0x60,
            0x70,	 0x60,
            0x70,	 0x60,
            0x78,	 0x60,
            0x78,	 0x60,
            0x6C,	 0x60,
            0x6C,	 0x60,
            0x64,	 0x60,
            0x66,	 0x60,
            0x66,	 0x60,
            0x63,	 0x60,
            0x63,	 0x60,
            0x61,	 0xE0,
            0x61,	 0xE0,
            0x60,	 0xE0,
            0x60,	 0xE0,
            0x60,	 0x60,
            0x00,	 0x00,
            0x00,	 0x00,
            0x00,	 0x00
        },
        { // Ś
            0x03,	 0x80,
            0x07,	 0x00,
            0x0C,	 0x00,
            0x00,	 0x00,
            0x1F,	 0x80,
            0x3F,	 0xC0,
            0x30,	 0xE0,
            0x60,	 0x60,
            0x60,	 0x00,
            0x60,	 0x00,
            0x70,	 0x00,
            0x3C,	 0x00,
            0x1F,	 0x00,
            0x07,	 0x80,
            0x01,	 0xC0,
            0x00,	 0xE0,
            0x00,	 0x60,
            0x60,	 0x60,
            0x70,	 0xE0,
            0x3F,	 0xC0,
            0x1F,	 0x80,
            0x00,	 0x00,
            0x00,	 0x00,
            0x00,	 0x00
        },
        { // Š
            0x19,	 0x80,
            0x0F,	 0x00,
            0x06,	 0x00,
            0x00,	 0x00,
            0x1F,	 0x80,
            0x3F,	 0xC0,
            0x30,	 0xE0,
            0x60,	 0x60,
            0x60,	 0x00,
            0x60,	 0x00,
            0x70,	 0x00,
            0x3C,	 0x00,
            0x1F,	 0x00,
            0x07,	 0x80,
            0x01,	 0xC0,
            0x00,	 0xE0,
            0x00,	 0x60,
            0x60,	 0x60,
            0x70,	 0xE0,
            0x3F,	 0xC0,
            0x1F,	 0x80,
            0x00,	 0x00,
            0x00,	 0x00,
            0x00,	 0x00
        },
        { // Ŭ
            0x19,	 0x80,
            0x19,	 0x80,
            0x1F,	 0x80,
            0x0F,	 0x00,
            0x60,	 0x60,
            0x60,	 0x60,
            0x60,	 0x60,
            0x60,	 0x60,
            0x60,	 0x60,
            0x60,	 0x60,
            0x60,	 0x60,
            0x60,	 0x60,
            0x60,	 0x60,
            0x60,	 0x60,
            0x60,	 0x60,
            0x60,	 0x60,
            0x60,	 0x60,
            0x60,	 0x60,
            0x30,	 0xE0,
            0x3F,	 0xC0,
            0x1F,	 0x80,
            0x00,	 0x00,
            0x00,	 0x00,
            0x00,	 0x00
        },
        { // Ź
            0x03,	 0x80,
            0x07,	 0x00,
            0x0C,	 0x00,
            0x00,	 0x00,
            0x7F,	 0xE0,
            0x7F,	 0xE0,
            0x00,	 0xE0,
            0x00,	 0xC0,
            0x01,	 0x80,
            0x01,	 0x80,
            0x03,	 0x00,
            0x03,	 0x00,
            0x06,	 0x00,
            0x0E,	 0x00,
            0x0C,	 0x00,
            0x18,	 0x00,
            0x38,	 0x00,
            0x30,	 0x00,
            0x70,	 0x00,
            0x7F,	 0xE0,
            0x7F,	 0xE0,
            0x00,	 0x00,
            0x00,	 0x00,
            0x00,	 0x00
        },
        { // Ž
            0x19,	 0x80,
            0x0F,	 0x00,
            0x06,	 0x00,
            0x00,	 0x00,
            0x7F,	 0xE0,
            0x7F,	 0xE0,
            0x00,	 0xE0,
            0x00,	 0xC0,
            0x01,	 0x80,
            0x01,	 0x80,
            0x03,	 0x00,
            0x03,	 0x00,
            0x06,	 0x00,
            0x0E,	 0x00,
            0x0C,	 0x00,
            0x18,	 0x00,
            0x38,	 0x00,
            0x30,	 0x00,
            0x70,	 0x00,
            0x7F,	 0xE0,
            0x7F,	 0xE0,
            0x00,	 0x00,
            0x00,	 0x00,
            0x00,	 0x00
        }
    };

/*
0	Ć	C4 86	!
1	Č	C4 8C	#
2	Ł	C5 81	$
3	Ĺ	C4 B9	%
4	Ń	C5 83	&
5	Ś	C5 9A	'
6	Š	C5 A0	/
7	Ŭ	C5 AC	;
8	Ź	C5 B9	?
9	Ž	C5 BD	@
*/

    // Define user-defined chars - diacritics
    if( ! fillCmdBuffer(5, CH__ESC, '&', 2, 0x5C, 0x5C) )   return false; //backslash
    if( ! fillCmdBuffer(48, diacritics[0]) )                return false;

    if( ! fillCmdBuffer(5, CH__ESC, '&', 2, '{', '{') )     return false;
    if( ! fillCmdBuffer(48, diacritics[1]) )                return false;

    if( ! fillCmdBuffer(5, CH__ESC, '&', 2, '$', '$') )     return false;
    if( ! fillCmdBuffer(48, diacritics[2]) )                return false;

    if( ! fillCmdBuffer(5, CH__ESC, '&', 2, '`', '`') )     return false;
    if( ! fillCmdBuffer(48, diacritics[3]) )                return false;

    if( ! fillCmdBuffer(5, CH__ESC, '&', 2, '&', '&') )     return false;
    if( ! fillCmdBuffer(48, diacritics[4]) )                return false;

    if( ! fillCmdBuffer(5, CH__ESC, '&', 2, '^', '^') )     return false;
    if( ! fillCmdBuffer(48, diacritics[5]) )                return false;

    if( ! fillCmdBuffer(5, CH__ESC, '&', 2, '}', '}') )     return false;
    if( ! fillCmdBuffer(48, diacritics[6]) )                return false;

    if( ! fillCmdBuffer(5, CH__ESC, '&', 2, '|', '|') )     return false;
    if( ! fillCmdBuffer(48, diacritics[7]) )                return false;

    if( ! fillCmdBuffer(5, CH__ESC, '&', 2, '~', '~') )     return false;
    if( ! fillCmdBuffer(48, diacritics[8]) )                return false;

    if( ! fillCmdBuffer(5, CH__ESC, '&', 2, '@', '@') )     return false;
    if( ! fillCmdBuffer(48, diacritics[9]) )                return false;

    if( ! sendCmdBuffer(10000) ) {
        return false;
    }


    timespec    t1, t2;


    clock_gettime( CLOCK_MONOTONIC, &t1 );

    while( true ) {
        int result = sp_output_waiting( SP );


        if( 0 == result ) {
            break;
        }
        if( result < 0 ) {
            lastError = CU_PRINT_ERR__PORT_SEND_DATA;

            break;
        }

        clock_gettime( CLOCK_MONOTONIC, &t2 );

        if( (t2.tv_sec - t1.tv_sec) > 10 ) {
            lastError = CU_PRINT_ERR__PORT_IO_TIMEOUT;

            break;
        }

        usleep( 10000 );
    }


    return ( CU_PRINT_ERR__NONE == lastError );
}
//--------------------------------------------------------------------

bool
TCU_DatecsBT::cancelPrintJob()
{
    lastError = CU_PRINT_ERR__UNSUPPORTED_FEATURE;

    return false;
}
//--------------------------------------------------------------------

bool
TCU_DatecsBT::sendRawDataBin( uint8_t* buffer, size_t bytesCount )
{
    return sendBytesToSP( buffer, bytesCount, 0 );
}
//--------------------------------------------------------------------
/*
bool
TCU_DatecsBT::feedPaper( TCU_FeedMode fpMode, int units )
{
    lastError = CU_PRINT_ERR__UNSUPPORTED_FEATURE;

    return false;
}
//--------------------------------------------------------------------
*/

bool
TCU_DatecsBT::changeCodeTable( unsigned char codeTable )
{
    clearCmdBuffer();

    fillCmdBuffer( 3, CH__ESC, 'u', codeTable );

    if( ! sendCmdBuffer(10000) ) {
        clearCmdBuffer();

        return false;
    }
    return true;
}
//--------------------------------------------------------------------
bool
TCU_DatecsBT::cutPaper( TCU_CutterType coType, int adv )
{
    lastError = CU_PRINT_ERR__NONE;

    return true;
}
//--------------------------------------------------------------------
/*
bool
TCU_DatecsBT::printBarCode( TCU_BarcodeType bcType, const char* bcData )
{
    int codeLen = strlen( bcData );


    clearCmdBuffer();

    switch( bcType ) {
        case bcType_Code128:
            // select character font for HRI characters: Font B
            if( ! fillCmdBuffer(3, CH__GS, 'f', 1) ) {
                return false;
            }
            //if( ! fillCmdBuffer(6, CH__GS, 'k', 73, codeLen + 2, 0x7B, 0x42) ) {
            if( ! fillCmdBuffer(4, CH__GS, 'k', 75, codeLen) ) {
                return false;
            }
            if( ! fillCmdBuffer(codeLen, bcData) ) {
                return false;
            }

            break;

        default:
            lastError = CU_PRINT_ERR__INVALID_BC_TYPE;

            return false;
    }

    if( ! sendCmdBuffer(10000) ) {
        clearCmdBuffer();

        return false;
    }

    return true;
}
//--------------------------------------------------------------------
*/
bool
TCU_DatecsBT::printBarCode( TCU_BarcodeType bcType, const char* bcData, ... )
{
    va_list ap;
    int     codeLen = strlen( bcData );


    clearCmdBuffer();
    va_start( ap, bcData );

    switch( bcType ) {
        case bcType_Code128:
            // select character font for HRI characters: Font B
            if( ! fillCmdBuffer(3, CH__GS, 'f', 1) ) {
                return false;
            }
            if( ! fillCmdBuffer(4, CH__GS, 'k', 75, codeLen) ) {
                return false;
            }
            if( ! fillCmdBuffer(codeLen, bcData) ) {
                return false;
            }

            break;

        case bcType_QR: {
            int     qrSize = va_arg( ap, int );
            int     qrEccl = va_arg( ap, int );


            if( (qrSize < 1) || (qrSize > 14) || (qrEccl < 1) || (qrEccl > 4) ) {
                lastError = CU_PRINT_ERR__INVALID_PROP_VAL;

                break;
            }
            if( codeLen > 448 ) {
                lastError = CU_PRINT_ERR__INVALID_PROP_VAL;

                break;
            }

            if( codeLen <= 0 ) {
                lastError = CU_PRINT_ERR__INVALID_PROP_VAL;

                break;
            }

            if( ! fillCmdBuffer(7, CH__GS, 'Q', 6, qrSize, qrEccl, codeLen, codeLen >> 8) ) {
                break;
            }
            if( ! fillCmdBuffer(codeLen, bcData) ) {
                break;
            }

            break;
        }

        case bcType_PDF417: {
            int     pdfModuleWidth = va_arg( ap, int ); //
            int     pdfHeight = va_arg( ap, int );
            int     pdfEccl = va_arg( ap, int );    // 1..9 (9 - auto)

            // for Datecs DPP250 pdfModuleWidth is 0xYZ where Y is Type, Z is EncMode
            // for Datecs DPP250 pdfHeight is Size (see documentation for printer)

            int     pdfType = (pdfModuleWidth >> 8) & 0x0F; // 0 - standard, 1 - truncated
            int     pdfEncMode = pdfModuleWidth & 0x0F; // 0 - automatic, 1 - binary




            if (   (pdfType < 0) || (pdfType > 1) \
                || (pdfEncMode < 0) || (pdfEncMode > 1) \
                || (pdfEccl < 0) || (pdfEccl > 9) \
                || (pdfHeight < 0) || (pdfHeight > 15) ) {
                lastError = CU_PRINT_ERR__INVALID_PROP_VAL;

                break;
            }
            if( codeLen > 384 ) {
                lastError = CU_PRINT_ERR__INVALID_PROP_VAL;

                break;
            }

/*            if( ! fillCmdBuffer(5, CH__GS, 0x70, 9, 0, 0) ) { // error correction level 9 and > - auto
                break;
            }
            if( ! fillCmdBuffer(3, CH__GS, 0x68, pdfHeight) ) { // height of code
                break;
            }
            if( ! fillCmdBuffer(3, CH__GS, 0x71, 4) ) { // line height
                break;
            }
            if( ! fillCmdBuffer(3, CH__GS, 0x77, 4) ) { // scale factor
                break;
            } */

            if( ! fillCmdBuffer(9, CH__GS, 0x51, 2, pdfType, pdfEncMode, pdfEccl, \
                pdfHeight, codeLen, codeLen >> 8) ) {
                break;
            }
            if( ! fillCmdBuffer(codeLen, bcData) ) {
                break;
            }

            break;
        }

        default:
            lastError = CU_PRINT_ERR__INVALID_BC_TYPE;

            break;
    }

    va_end( ap );

    if( CU_PRINT_ERR__NONE != lastError ) {
        return false;
    }

    if( ! sendCmdBuffer(10000) ) {
        clearCmdBuffer();

        return false;
    }

    return true;
}
//--------------------------------------------------------------------


///*******************************************************************
///PROTECTED METHODS OF DATECS_BT_PRINTER CLASS
///*******************************************************************


///*******************************************************************
///PRIVATE METHODS OF DATECS_BT_PRINTER CLASS
///*******************************************************************

const char*
TCU_DatecsBT::getVersionStr()
{
/*    char    txBuf[] = { CH__ESC, 's', '2' };
    char    rxBuf[16];
    int     ioResult;


    ioResult = sp_blocking_write( SP, txBuf, 3, 1000 );
    if( ioResult < 3 ) {
        lastError = ( ioResult < 0 )? CU_PRINT_ERR__PORT_WRITE: CU_PRINT_ERR__PORT_IO_TIMEOUT;

        return "";
    }

    ioResult = sp_blocking_read( SP, rxBuf, 2, 1000 );
    if( ioResult < 2 ) {
        lastError = ( ioResult < 0 )? CU_PRINT_ERR__PORT_READ: CU_PRINT_ERR__PORT_IO_TIMEOUT;

        return "";
    }

    size_t  rxBytesCount = 2 * rxBuf[0] * rxBuf[1];
    char*   rxLogo = new char[ rxBytesCount ];


    ioResult = sp_blocking_read( SP, rxLogo, rxBytesCount, 15000 );
    if( ioResult < (int) rxBytesCount ) {
        lastError = ( ioResult < 0 )? CU_PRINT_ERR__PORT_READ: CU_PRINT_ERR__PORT_IO_TIMEOUT;
    }

    delete[] rxLogo;
*/

    return DATECS_TP__VERSION_STRING;
}
//--------------------------------------------------------------------

bool
TCU_DatecsBT::finalizePrintJob( json_object* jPJob )
{
/*    unsigned int    id = 0xffff & rand();
    char            idStr[8];


    sprintf( idStr, "%.4X", id );

    char        txBuf[16] = { CH__GS, '(', 'H', 6, 0, 48, 48, idStr[0], idStr[1], idStr[2], idStr[3] };
    int         ioResult = sp_blocking_write( SP, txBuf, 11, 1000 );


    lastError = CU_PRINT_ERR__NONE;

    if( ioResult < 11 ) {
        lastError = ( ioResult < 0 )? CU_PRINT_ERR__PORT_WRITE: CU_PRINT_ERR__PORT_IO_TIMEOUT;

        return false;
    }
    if( SP_OK != sp_flush(SP, SP_BUF_INPUT) ) {
        lastError = CU_PRINT_ERR__PORT_FLUSH_DATA;

        return false;
    }

    char    wnd[2] = { 0, 0 };
    uint8_t asb[4], offs = 0;


    do {
        if( (ioResult = sp_input_waiting(SP)) < 0 ) {
            lastError = CU_PRINT_ERR__PORT_READ;

            break;
        }
        if( 0 == ioResult ) continue;

        if( (ioResult = sp_nonblocking_read(SP, wnd, 1)) < 0 ) {
            lastError = CU_PRINT_ERR__PORT_READ;

            break;
        }

        if( ioResult ) {
            if( 0x3722 == (*(uint16_t*) wnd) ) {
                char    buf[8];


                ioResult = sp_blocking_read( SP, buf, 5, 1000 );

                if( ioResult < 5 ) {
                    lastError = ( ioResult < 0 )? CU_PRINT_ERR__PORT_READ: CU_PRINT_ERR__PORT_IO_TIMEOUT;
                }

                if( 0 == strncmp(idStr, buf, 4) ) {
                    break;
                }
            }
            else {
                wnd[1] = wnd[0];

                if( offs < sizeof(asb) ) {
                    asb[offs++] = wnd[0];
                }

                if( sizeof(asb) == offs ) {
                    if( (0x10 != (asb[0] & 0x93)) || (0x00 != (asb[1] & 0x90)) || \
                        (0x00 != (asb[2] & 0xF0)) || (0x0F != (asb[3] & 0x9F)) ) {
                        lastError = CU_PRINT_ERR__INVALID_ASB_DATA;

                        break;
                    }

                    offs = 0;

                    if( asb[0] & 0x08 ) {
                    // offline state
                        if( ! this->cancelPrintJob() ) {
                            break;
                        }

                        if( asb[0] & 0x20 ) {
                            lastError = CU_PRINT_ERR__COVER_IS_OPENED;
                        }
                        else
                        if( asb[2] & 0x0C ) {
                            lastError = CU_PRINT_ERR__PAPER_END;
                        }
                        else lastError = CU_PRINT_ERR__OFFLINE_STATE;

                        break;
                    }
                }
            }
        }

        usleep( 10000 );
    }
    while( true );
*/
lastError = CU_PRINT_ERR__NONE;

    return ( CU_PRINT_ERR__NONE == lastError );
}
//--------------------------------------------------------------------

bool
TCU_DatecsBT::updateState()
{
    char    txBuf[] = { CH__ESC, 'v' };
    char    rxByte = 0;
    int     ioResult;


    if( SP_OK != sp_flush(SP, SP_BUF_INPUT) ) {
        lastError = CU_PRINT_ERR__PORT_FLUSH_DATA;
        return false;
    }

    ioResult = sp_blocking_write( SP, txBuf, sizeof(txBuf), 1000 );
    if( ioResult < (int) sizeof(txBuf) ) {
        lastError = ( ioResult < 0 )? CU_PRINT_ERR__PORT_WRITE: CU_PRINT_ERR__PORT_IO_TIMEOUT;

        return false;
    }

    ioResult = sp_blocking_read( SP, &rxByte, 1, 1000 );
    if( ioResult < 1 ) {
        lastError = ( ioResult < 0 )? CU_PRINT_ERR__PORT_READ: CU_PRINT_ERR__PORT_IO_TIMEOUT;

        return false;
    }


    State.errorsDetected = false;
    State.recoverableError = false;
    State.unrecoverableError = false;
    State.coverIsOpened = false;
    State.paperEndDetected  = false;
    State.paperNearEndDetected = false;
    State.online = true;

    if( rxByte & 0x04 ) {
        State.coverIsOpened = true;
        State.paperEndDetected  = true;
        State.online = false;
    }
    if( rxByte & 0x08 ) {
        State.errorsDetected = true;
        State.recoverableError = true;
        State.overheatDetected = true;
    }
    if( rxByte & 0x40 ) {
        State.errorsDetected = true;
        State.unrecoverableError = true;
        State.lowBatteryVoltage = true;
    }


    return true;
}
//--------------------------------------------------------------------

bool
TCU_DatecsBT::afterProcessLine( json_object* jLine, int idx )
{
    if( this->updateState() ) {
        if( ! State.online ) {
            lastError = CU_PRINT_ERR__OFFLINE_STATE;

            return false;
        }
        else return true;
    }
    else return false;
}
//--------------------------------------------------------------------

bool
TCU_DatecsBT::putCmd_Print()
{
    return fillCmdBuffer( 1, '\n' );
}
//--------------------------------------------------------------------

bool
TCU_DatecsBT::putCmd_SetAlignment( TCU_PropertyVal propVal )
{
    int alProp;


    switch( propVal ) {
        case pv_Left:   alProp = 0; break;
        case pv_Center: alProp = 1; break;
        case pv_Right:  alProp = 2; break;

        default:
            lastError = CU_PRINT_ERR__INVALID_PROP_VAL;

            return false;
    }


    return fillCmdBuffer( 3, CH__ESC, 'a', alProp );
}
//--------------------------------------------------------------------
bool
TCU_DatecsBT::putCmd_SetBarcodeWidth( int widthCode )
{
    if( (widthCode < 2) || (widthCode > 4) ) {
        widthCode = 3;
    }

    return fillCmdBuffer( 3, CH__GS, 'w', widthCode );
}
//--------------------------------------------------------------------

bool
TCU_DatecsBT::putCmd_SetBarcodeHeight( int heightDots )
{
    if( (heightDots < 1) || (heightDots > 255) ) {
        heightDots = 162;
    }

    return fillCmdBuffer( 3, CH__GS, 'h', heightDots );
}
//--------------------------------------------------------------------

bool
TCU_DatecsBT::putCmd_SetBarcodeHriPos( TCU_PropertyVal propVal )
{
    int hriProp;


    switch( propVal ) {
        case pv_None:   hriProp = 0; break;
        case pv_Above:  hriProp = 1; break;
        case pv_Below:  hriProp = 2; break;
        case pv_Both:   hriProp = 3; break;

        default:
            lastError = CU_PRINT_ERR__INVALID_PROP_VAL;

            return false;
    }

    return fillCmdBuffer( 3, CH__GS, 'H', hriProp );
}

//--------------------------------------------------------------------

bool
TCU_DatecsBT::putCmd_SetCharacterSize( TCU_PropertyVal propVal )
{
    switch( propVal ) {
        case pv_StdSize:    printingModeOfTextData &= 0xCF; break;
        case pv_DblSize:    printingModeOfTextData |= 0x30; break;
        case pv_DblHeight:
            printingModeOfTextData &= 0xDF;
            printingModeOfTextData |= 0x10;
            break;

        case pv_DblWidth:
            printingModeOfTextData &= 0xEF;
            printingModeOfTextData |= 0x20;
            break;

        default:
            lastError = CU_PRINT_ERR__INVALID_PROP_VAL;

            return false;
    }

    return fillCmdBuffer( 3, CH__ESC, '!', printingModeOfTextData );
}
//--------------------------------------------------------------------
//--------------------------------------------------------------------
char*
TCU_DatecsBT::allocBuffer( const char* str )
{
    if( NULL == str ) {
        return NULL;
    }

    char*   buf = (char*) malloc( strlen(str) + 1 );

    if( NULL != buf ) {
        strcpy( buf, str );

        size_t  i = 0, n;

        while( (n = strlen(&buf[i])) >= 2) {
    // New substitutions for diacritics
            if( (buf[i] == 0xC4) && (buf[i+1] == 0x86) ) {
                buf[i] = 0x5C;
                memmove( &buf[i+1], &buf[i+2], n - 1 );
            }
            else
            if( (buf[i] == 0xC4) && (buf[i+1] == 0x8C) ) {
                buf[i] = '{';
                memmove( &buf[i+1], &buf[i+2], n - 1 );
            }
            else
            if( (buf[i] == 0xC5) && (buf[i+1] == 0x81) ) {
                buf[i] = '$';
                memmove( &buf[i+1], &buf[i+2], n - 1 );
            }
            else
            if( (buf[i] == 0xC4) && (buf[i+1] == 0xB9) ) {
                buf[i] = '`';
                memmove( &buf[i+1], &buf[i+2], n - 1 );
            }
            else
            if( (buf[i] == 0xC5) && (buf[i+1] == 0x83) ) {
                buf[i] = '&';
                memmove( &buf[i+1], &buf[i+2], n - 1 );
            }
            else
            if( (buf[i] == 0xC5) && (buf[i+1] == 0x9A) ) {
                buf[i] = '^';
                memmove( &buf[i+1], &buf[i+2], n - 1 );
            }
            else
            if( (buf[i] == 0xC5) && (buf[i+1] == 0xA0) ) {
                buf[i] = '}';
                memmove( &buf[i+1], &buf[i+2], n - 1 );
            }
            else
            if( (buf[i] == 0xC5) && (buf[i+1] == 0xAC) ) {
                buf[i] = '|';
                memmove( &buf[i+1], &buf[i+2], n - 1 );
            }
            else
            if( (buf[i] == 0xC5) && (buf[i+1] == 0xB9) ) {
                buf[i] = '~';
                memmove( &buf[i+1], &buf[i+2], n - 1 );
            }
            else
            if( (buf[i] == 0xC5) && (buf[i+1] == 0xBD) ) {
                buf[i] = '@';
                memmove( &buf[i+1], &buf[i+2], n - 1 );
            }
            else i++;

        }
    }

    return buf;
}
//--------------------------------------------------------------------

