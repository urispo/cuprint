#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <sys/time.h>
#include <string>
#include "cu_print.h"

#define CU_PRINT_MAIN_VERSION_STRING    "2"

std::string recodeText( const char* text, const char* srcEnc, const char* dstEnc );

///*******************************************************************
///PUBLIC METHODS OF BASE_PRINTER CLASS
///*******************************************************************

TCU_BasePrinter::TCU_BasePrinter( const char* portName ) :
ifceCS( "UTF-8" )
{
    modelId = CU_PRINT_MODEL_ID__UNDEF;
    hconv = (iconv_t)(-1);
    SP = NULL;
    txtimeout = -1;

    lastError = CU_PRINT_ERR__NONE;

    if( NULL != portName ) {
        if( SP_OK != sp_get_port_by_name(portName, &SP) ) {
            lastError = CU_PRINT_ERR__PORT_NAME;

            return;
        }

        if( SP_OK != sp_open(SP, SP_MODE_READ_WRITE) ) {
            if( NULL != SP ) {
                sp_free_port( SP );
                SP = NULL;
            }

            lastError = CU_PRINT_ERR__PORT_OPEN;

            return;
        }
    }

    clearCmdBuffer();
}
//--------------------------------------------------------------------

TCU_BasePrinter::~TCU_BasePrinter()
{
    if( NULL != SP ) {
        sp_close( SP );
        sp_free_port( SP );

        SP = NULL;
    }

    if( ((iconv_t)-1) != hconv ) {
        iconv_close( hconv );
        hconv = (iconv_t)(-1);
    }
}
//--------------------------------------------------------------------

bool
TCU_BasePrinter::getState( TCU_PrinterState* State )
{
    this->State.dataIsValid = updateState();

    if( NULL != State ) {
        *State = this->State;
    }

    return ( CU_PRINT_ERR__NONE == lastError );
}
//--------------------------------------------------------------------

std::string
TCU_BasePrinter::getVersion()
{
    std::string Version = CU_PRINT_MAIN_VERSION_STRING;


    Version.append(":");
    Version.append( getVersionStr() );

    return Version;
}
//--------------------------------------------------------------------

bool
TCU_BasePrinter::setProperty( TCU_PropertyId propId, TCU_PropertyVal propVal, ... )
{
    va_list     vl;
//    int         i_val = 0;


    lastError = CU_PRINT_ERR__NONE;
    va_start( vl, propVal );

    switch( propId ) {
        case propId_Alignment:      putCmd_SetAlignment( propVal );                 break;

        case propId_BarCodeWidth:   putCmd_SetBarcodeWidth( va_arg(vl, int) );      break;
        case propId_BarCodeHeight:  putCmd_SetBarcodeHeight( va_arg(vl, int) );     break;
        case propId_BarCodeHRI:     putCmd_SetBarcodeHriPos( propVal );             break;
        case propId_CharacterSize:  putCmd_SetCharacterSize( propVal );             break;

        default:
            lastError = CU_PRINT_ERR__INVALID_PROP_ID;
            break;
    }

    va_end( vl );

    if( CU_PRINT_ERR__NONE == lastError ) {
        sendCmdBuffer();
    }


    return ( CU_PRINT_ERR__NONE == lastError )? true: false;
}
//--------------------------------------------------------------------

bool
TCU_BasePrinter::doPrintJob( json_object* jPrintJob )
{
    json_object*    jPJData = json_object_object_get( jPrintJob, "data" );
    json_object*    jPJSets = json_object_object_get( jPrintJob, "settings" );


    if( (NULL == jPJData) || (json_type_array != json_object_get_type(jPJData)) ) {
        lastError = CU_PRINT_ERR__INVALID_PJ_STRUCT;

        return false;
    }

    lastError = CU_PRINT_ERR__NONE;

    ptColumns = json_object_get_int( json_object_object_get(jPrintJob, "columns") );
    ptPaperWidth = json_object_get_int( json_object_object_get(jPrintJob, "paperWidth") );
//printf( "%s\n", json_object_to_json_string_ext(jPrintJob, JSON_C_TO_STRING_PRETTY) );

// commented due to incorrect setting comma and period in next print document
// if locale in doc_print_template is other than en_US
/*    if( NULL == setlocale(LC_ALL, json_object_get_string(json_object_object_get(jPrintJob, "locale"))) ) {
        std::string EMsg("unable to set given locale (");

        EMsg += json_object_get_string( json_object_object_get(jPrintJob, "locale") );
        EMsg += ")";

        fprintf( stderr, "%s\n", EMsg.c_str() );
    } */
    json_object_object_add( jPrintJob, "pjProgress", json_object_new_int(0) );

    if( ! beforePrintJobStart() ) {
        return false;
    }

    int     pjStartIndex = json_object_get_int( json_object_object_get(jPrintJob, "pjStartPoint") );
    bool    emcyExit = false;


    for( size_t i = 0; (i < json_object_array_length(jPJData)) && !emcyExit; i++ ) {
        json_object*    jLine = json_object_array_get_idx( jPJData, i );


        switch( json_object_get_type(jLine) ) {
            case json_type_int:
                if( (NULL != jPJSets) && (json_type_array == json_object_get_type(jPJSets)) ) {
                    json_object*    jItem = json_object_array_get_idx( jPJSets, json_object_get_int(jLine) );


                    if( NULL == jItem ) break;

                    if( json_object_is_type(jItem, json_type_object) ) {
                        applyStyle( jItem );
                    }
                    else
                    if( ((int) i) < pjStartIndex ) {
                        continue;
                    }
                    else
                    if( json_object_is_type(jItem, json_type_array) ) {
                        const char* action = json_object_get_string( json_object_array_get_idx(jItem, 0) );
                        size_t      array_len = json_object_array_length( jItem );


                        if( 0 == strcmp(action, "aPrintBitImage") ) {
                            printBitImage( json_object_get_int(json_object_array_get_idx(jItem, 1)) );
                        }
                        else
                        if( 0 == strcmp(action, "aPrintBarcode") ) {
                            const char* type = json_object_get_string( json_object_array_get_idx(jItem, 1) );


                            if( (0 == strcmp(type, "code128")) && (2 == array_len) ) {
                                printBarCode( bcType_Code128, json_object_get_string(json_object_array_get_idx(jPJData, ++i)) );
                            }
                            else
                            if( (0 == strcmp(type, "QR")) && (4 == array_len)) {
                                int qrSize = json_object_get_int( json_object_array_get_idx(jItem, 2) );
                                int qrEccl = json_object_get_int( json_object_array_get_idx(jItem, 3) );

                                printBarCode( bcType_QR, json_object_get_string(json_object_array_get_idx(jPJData, ++i)), qrSize, qrEccl );
                            }
                            else
                            if( (0 == strcmp(type, "PDF417")) && (5 == array_len)) {
                                int pdfModuleWidth = json_object_get_int( json_object_array_get_idx(jItem, 2) );
                                int pdfHeight = json_object_get_int( json_object_array_get_idx(jItem, 3) );
                                int pdfEccl = json_object_get_int( json_object_array_get_idx(jItem, 4) );
                                // for Datecs DPP250 pdfModuleWidth is 0xYZ where Y is Type, Z is EncMode
                                // for Datecs DPP250 pdfHeight is Size (see documentation for printer)
                                printBarCode( bcType_PDF417, json_object_get_string(json_object_array_get_idx(jPJData, ++i)), pdfModuleWidth, pdfHeight, pdfEccl );
                            }
                        }
                        else
                        if( 0 == strcmp(action, "aNewLine") ) {
                            if( array_len != 2 ) continue;
                            //json_object_get_int(json_object_array_get_idx(jItem, 1))
                            putCmd_Print();
                        }
                        else
                        if( 0 == strcmp(action, "aCutPaper") ) {
                            TCU_CutterType  type;
                            int             n = -1;


                            switch( array_len ) {
                                case 3: n = json_object_get_int( json_object_array_get_idx(jItem, 2) );
                                case 2: {
                                    const char*     cutType = json_object_get_string( json_object_array_get_idx(jItem, 1) );


                                    if( 0 == strcmp(cutType, "full") ) {
                                        type = coType_Full;
                                    }
                                    else
                                    if( 0 == strcmp(cutType, "partial") ) {
                                        type = coType_Partial;
                                    }
                                    else type = coType_Undef;

                                    break;
                                }

                                default: continue;
                            }

                            if( ! cutPaper(type, n) ) {
                                continue;
                            }
                        }
                        else
                        if( 0 == strcmp(action, "aFeedPaper") ) {
                            if( array_len != 3 ) continue;

                            TCU_FeedMode    mode;
                            const char*     feedUnits = json_object_get_string( json_object_array_get_idx(jItem, 1) );

                            if( 0 == strcmp(feedUnits, "lines") ) {
                                mode = fpMode_Lines;
                            }
                            else
                            if( 0 == strcmp(feedUnits, "dots") ) {
                                mode = fpMode_Dots;
                            }
                            else mode = fpMode_Undef;

                            feedPaper( mode, json_object_get_int(json_object_array_get_idx(jItem, 2)) );
                        }
                        else
                        if( 0 == strcmp(action, "aSendRawData") ) {
                            if( array_len != 2 ) continue;

                            const char*     data = json_object_get_string( json_object_array_get_idx(jItem,1) );

                            if( ! sendRawData(data) ) {
                                continue;
                            }
                        }
                        else
                        if( 0 == strcmp(action, "aCodeTable") ) {
                            if( array_len != 2 ) continue;

                            changeCodeTable( (unsigned char) json_object_get_int(json_object_array_get_idx(jItem, 1)) );
                        }
                        else break;
                    }
                    else continue;
                }

                break;


            default:
                if( ((int) i) < pjStartIndex ) {
                    continue;
                }

                if( ! printText(json_object_get_string(jLine)) ) {
                    emcyExit = true;
                }
                if( ! afterProcessLine(jLine, i) ) {
                    emcyExit = true;
                }

                break;
        }
    }


    return afterPrintJobFinish( finalizePrintJob(jPrintJob) );
}
//--------------------------------------------------------------------

bool
TCU_BasePrinter::printText( const char* text )
{
    size_t  txtsz = strlen( text );
    size_t  offs = 0;


    clearCmdBuffer();

    do {
        offs += fillCmdBuffer( &text[offs] );

        if( CU_PRINT_ERR__NONE != lastError ) {
            return false;
        }

        if( ! sendCmdBuffer(txtimeout) ) {
            return false;
        }
    }
    while( offs < txtsz );

    if( ! putCommand(cmdId_Print) ) {
        return false;
    }

    return sendCmdBuffer( 1000 );
}
//--------------------------------------------------------------------

bool
TCU_BasePrinter::sendRawData( const char* data )
{
    size_t      datalen = strlen( data );
    size_t      bufSz = datalen/2;
    uint8_t*    buf = (uint8_t*) calloc( 1 + bufSz, 1 );
    std::string Data;


    if( datalen % 2 ) {
        Data = "0";
        bufSz++;
    }
    Data += data;

    for( size_t i = 0, j = 0; j < bufSz; j++ ) {
        char tmp[4];


        tmp[0] = Data[i++];
        tmp[1] = Data[i++];
        tmp[2] = '\0';
        buf[j] = strtoul( tmp, NULL, 16 );
    }

    bool result = sendRawDataBin( buf, bufSz );


    free( buf );

    return result;
}
//--------------------------------------------------------------------

bool
TCU_BasePrinter::feedPaper( TCU_FeedMode fpMode, int units )
{
    if( units < 0 ) {
        lastError = CU_PRINT_ERR__UNSUPPORTED_FEATURE;

        return false;
    }

    clearCmdBuffer();

    switch( fpMode ) {
        case fpMode_Lines:  fillCmdBuffer( 3, CH__ESC, 'd', units ); break;
        case fpMode_Dots:   fillCmdBuffer( 3, CH__ESC, 'J', units ); break;

        default: lastError = CU_PRINT_ERR__ARG_IS_OUT_OF_RANGE;  return false;
    }

    return sendCmdBuffer( 1000 );
}
//--------------------------------------------------------------------

bool
TCU_BasePrinter::changeCodeTable( unsigned char codeTable )
{
    clearCmdBuffer();

    fillCmdBuffer( 3, CH__ESC, 't', codeTable );

    return sendCmdBuffer( 1000 );
}
//--------------------------------------------------------------------
bool
TCU_BasePrinter::cutPaper( TCU_CutterType coType, int adv )
{
    int type;


    clearCmdBuffer();

    switch( coType ) {
        case coType_Full:       type = ( adv < 0 )? 0: 65;  break;
        case coType_Partial:    type = ( adv < 0 )? 1: 66;  break;

        default:
            lastError = CU_PRINT_ERR__ARG_IS_OUT_OF_RANGE;  return false;
    }

    if( adv < 0 ) {
        fillCmdBuffer( 3, CH__GS, 'V', type );
    }
    else fillCmdBuffer( 4, CH__GS, 'V', type, adv );

    if( CU_PRINT_ERR__NONE != lastError ) {
        return false;
    }

    if( ! sendCmdBuffer(1000) ) {
        clearCmdBuffer();

        return false;
    }

    return true;
}
//--------------------------------------------------------------------

bool
TCU_BasePrinter::printBitImage( int mode )
{
    clearCmdBuffer();

    if( ! fillCmdBuffer(3, CH__GS, '/', mode) ) {
        return false;
    }

    return sendCmdBuffer( 1000 );
}
//--------------------------------------------------------------------



///*******************************************************************
///PROTECTED METHODS OF BASE_PRINTER CLASS
///*******************************************************************

TCU_PortSettings
TCU_BasePrinter::getPortSettings( json_object* jConfig )
{
    TCU_PortSettings    PortSettings = DefaultPortSettings;
    json_object*        jValue = NULL;


    if( json_object_object_get_ex(jConfig, "baudrate", &jValue) ) {
        PortSettings.baudrate = json_object_get_int( jValue );
    }
    if( json_object_object_get_ex(jConfig, "databits", &jValue) ) {
        PortSettings.databits = json_object_get_int( jValue );
    }
    if( json_object_object_get_ex(jConfig, "stopbits", &jValue) ) {
        PortSettings.stopbits = json_object_get_int( jValue );
    }
    if( json_object_object_get_ex(jConfig, "parity", &jValue) ) {
        if( 0 == strcmp("NONE", json_object_get_string(jValue)) ) {
            PortSettings.parity = SP_PARITY_NONE;
        }
        else
        if( 0 == strcmp("EVEN", json_object_get_string(jValue)) ) {
            PortSettings.parity = SP_PARITY_EVEN;
        }
        else
        if( 0 == strcmp("ODD", json_object_get_string(jValue)) ) {
            PortSettings.parity = SP_PARITY_ODD;
        }
        else
        if( 0 == strcmp("MARK", json_object_get_string(jValue)) ) {
            PortSettings.parity = SP_PARITY_MARK;
        }
        else
        if( 0 == strcmp("SPACE", json_object_get_string(jValue)) ) {
            PortSettings.parity = SP_PARITY_SPACE;
        }
        else PortSettings.parity = SP_PARITY_INVALID;
    }
    if( json_object_object_get_ex(jConfig, "flowcontrol", &jValue) ) {
        if( 0 == strcmp("NONE", json_object_get_string(jValue)) ) {
            PortSettings.flowcontrol = SP_FLOWCONTROL_NONE;
        }
        else
        if( 0 == strcmp("DTRDSR", json_object_get_string(jValue)) ) {
            PortSettings.flowcontrol = SP_FLOWCONTROL_DTRDSR;
        }
        else
        if( 0 == strcmp("RTSCTS", json_object_get_string(jValue)) ) {
            PortSettings.flowcontrol = SP_FLOWCONTROL_RTSCTS;
        }
        else
        if( 0 == strcmp("XONXOFF", json_object_get_string(jValue)) ) {
            PortSettings.flowcontrol = SP_FLOWCONTROL_XONXOFF;
        }
        else PortSettings.flowcontrol = SP_FLOWCONTROL_NONE;
    }

    return PortSettings;
}
//--------------------------------------------------------------------

bool
TCU_BasePrinter::configPort( const TCU_PortSettings& Settings )
{
    lastError = CU_PRINT_ERR__NONE;

    do {
        if( SP_OK != sp_set_baudrate(SP, Settings.baudrate) ) {
            lastError = CU_PRINT_ERR__PORT_CONFIG;
            break;
        }

        if( SP_OK != sp_set_bits(SP, Settings.databits) ) {
            lastError = CU_PRINT_ERR__PORT_CONFIG;
            break;
        }

        if( SP_OK != sp_set_parity(SP, Settings.parity) ) {
            lastError = CU_PRINT_ERR__PORT_CONFIG;
            break;
        }

        if( SP_OK != sp_set_stopbits(SP, Settings.stopbits) ) {
            lastError = CU_PRINT_ERR__PORT_CONFIG;
            break;
        }


        bool    result = false;
//        sp_return   ec;

        switch( Settings.flowcontrol ) {
            case SP_FLOWCONTROL_NONE:
                if( SP_OK != sp_set_dtr(SP, SP_DTR_ON) ) {
                    break;
                }
                if( SP_OK != sp_set_rts(SP, SP_RTS_ON) ) {
                    break;
                }
                if( SP_OK != sp_set_dsr(SP, SP_DSR_IGNORE) ) {
                    break;
                }
                if( SP_OK != sp_set_cts(SP, SP_CTS_IGNORE) ) {
                    break;
                }

                result = true;
                break;

            case SP_FLOWCONTROL_DTRDSR:
                if( SP_OK != sp_set_dtr(SP, SP_DTR_FLOW_CONTROL) ) {
                    break;
                }
                if( SP_OK != sp_set_dsr(SP, SP_DSR_FLOW_CONTROL) ) {
                    break;
                }

                result = true;
                break;

            default: break;
        }

        if( !result ) {
            lastError = CU_PRINT_ERR__PORT_CONFIG;
            break;
        }
        if( SP_OK != sp_set_flowcontrol(SP, Settings.flowcontrol) ) {
            lastError = CU_PRINT_ERR__PORT_CONFIG;
            break;
        }

        if( SP_OK != sp_flush(SP, SP_BUF_BOTH) ) {
            lastError = CU_PRINT_ERR__PORT_CONFIG;
            break;
        }
    }
    while( false );

    if( CU_PRINT_ERR__NONE == lastError ) {
        UsedPortSettings = Settings;
        txtimeout = 1000 + ( 100000.0 / (double) Settings.baudrate ) * sizeof( cmdBuffer );

        return true;
    }

    sp_close( SP );
    sp_free_port( SP );
    SP = NULL;

    return false;
}
//--------------------------------------------------------------------
bool
TCU_BasePrinter::sendBytesToSP( uint8_t* buffer, size_t bytesCount, int timeout_ms )
{
    if( timeout_ms < 0 ) {
    // non-blocking mode
        lastError = CU_PRINT_ERR__UNSUPPORTED_FEATURE;

        return false;
    }

    int result = sp_blocking_write( SP, buffer, bytesCount, timeout_ms );


    if( result < 0 ) {
        lastError = CU_PRINT_ERR__PORT_WRITE;
    }
    else
    if( ((unsigned int) result) < bytesCount ) {
        lastError = CU_PRINT_ERR__PORT_IO_TIMEOUT;
    }
    else lastError = CU_PRINT_ERR__NONE;


    return ( CU_PRINT_ERR__NONE == lastError );
}
//--------------------------------------------------------------------

void
TCU_BasePrinter::clearCmdBuffer()
{
//    lastError = CU_PRINT_ERR__NONE;

    cmdBufferOffs = 0;
}
//--------------------------------------------------------------------

size_t
TCU_BasePrinter::fillCmdBuffer( size_t cmdSz, ... )
{
    size_t  i = 0;
    va_list vl;


    lastError = CU_PRINT_ERR__NONE;

    if( ((int) (sizeof(cmdBuffer) - cmdBufferOffs - cmdSz)) > 0 ) {
        va_start( vl, cmdSz );

        for( ; i < cmdSz; i++ ) {
            cmdBuffer[ cmdBufferOffs++ ] = (uint8_t) va_arg( vl, int );
        }

        va_end( vl );
    }
    else lastError = CU_PRINT_ERR__BUFFER_SPACE;

    return i;
}
//--------------------------------------------------------------------

size_t
TCU_BasePrinter::fillCmdBuffer( size_t cmdSz, const void* buf )
{
    size_t  i = 0;


    lastError = CU_PRINT_ERR__NONE;

    if( ((int) (sizeof(cmdBuffer) - cmdBufferOffs - cmdSz)) > 0 ) {
        for( uint8_t* src = (uint8_t*) buf; i < cmdSz; i++ ) {
            cmdBuffer[ cmdBufferOffs++ ] = *src++;
        }
    }
    else lastError = CU_PRINT_ERR__BUFFER_SPACE;

    return i;
}
//--------------------------------------------------------------------

size_t
TCU_BasePrinter::fillCmdBuffer( const char* text )
{
    size_t  textsz = strlen( text );


    lastError = CU_PRINT_ERR__NONE;

    if( ((iconv_t)-1) != hconv ) {
        char*       ibuf = allocBuffer( text );
        char* const src = ibuf;
        char*       obuf = (char*) &cmdBuffer[ cmdBufferOffs ];
        size_t      ibufsz = strlen( ibuf );
        size_t      obufsz = sizeof( cmdBuffer ) - cmdBufferOffs;


        errno = 0;
        iconv( hconv, NULL, NULL, &obuf, &obufsz );

        if( errno ) {
            lastError = CU_PRINT_ERR__ENC_SHIFT_SEQUENCE;
            clearCmdBuffer();
            free( src );

            return 0;
        }
        cmdBufferOffs += ( sizeof(cmdBuffer) - obufsz );

        iconv( hconv, &ibuf, &ibufsz, &obuf, &obufsz );

        if( ibufsz ) {
            switch( errno ) {
                case EILSEQ: // invalid multibyte sequence is encountered in the input
                    lastError = CU_PRINT_ERR__ENC_ISYMBOL_INVALID;
                    break;

                case EINVAL: // incomplete multibyte sequence is encountered in the input
                    lastError = CU_PRINT_ERR__ENC_ISYMBOL_INCOMPL;
                    break;

                case E2BIG: // output buffer has no more room for the next converted character
                    break;

                default:
                    clearCmdBuffer();
                    lastError = CU_PRINT_ERR__ENC_UNKNOWN_CRASH;
                    free( src );

                    return 0;
            }
        }
        cmdBufferOffs += ( sizeof(cmdBuffer) - obufsz );
        free( src );


        return ( textsz - ibufsz );
    }

    size_t  bytesCount = ( textsz < (sizeof(cmdBuffer) - cmdBufferOffs) )? textsz: sizeof( cmdBuffer ) - cmdBufferOffs;


    memcpy( cmdBuffer, text, bytesCount );
    cmdBufferOffs += bytesCount;


    return bytesCount;
}
//--------------------------------------------------------------------

bool
TCU_BasePrinter::putCommand( TCU_CommandId cmdId, ... )
{
    lastError = CU_PRINT_ERR__NONE;

    switch( cmdId ) {
        case cmdId_Print:       putCmd_Print();     break;

        default:
            lastError = CU_PRINT_ERR__INVALID_CMD_ID;
            break;
    }

    return ( CU_PRINT_ERR__NONE == lastError );
}
//--------------------------------------------------------------------

bool
TCU_BasePrinter::sendCmdBuffer( int timeout_ms, bool clearAfterSend )
{
//    struct timeval  t1, t2, rsub;
    timespec    t1, t2;
    size_t      sentBytes = 0;


//    gettimeofday( &t1, NULL );
    clock_gettime( CLOCK_MONOTONIC, &t1 );

    lastError = CU_PRINT_ERR__NONE;


    do {
        int nbwr_res = sp_nonblocking_write( SP, &cmdBuffer[ sentBytes ], cmdBufferOffs - sentBytes );


        if( nbwr_res < 0 ) {
            lastError = CU_PRINT_ERR__PORT_WRITE;
//            sp_flush( SP, SP_BUF_OUTPUT );
            break;
        }
        else sentBytes += ( (size_t) nbwr_res );

        while( sp_output_waiting(SP) > 0 ) {
            usleep( 1000 );

            if( timeout_ms >= 0 ) {
//                gettimeofday( &t2, NULL );
                clock_gettime( CLOCK_MONOTONIC, &t2 );

                int dt = (t2.tv_sec * 1000 + t2.tv_nsec / 1000000) - (t1.tv_sec * 1000 + t1.tv_nsec / 1000000);
                //timersub( &t2, &t1, &rsub );

//                if( (int) ((rsub.tv_sec * 1000) + (rsub.tv_usec / 1000)) > timeout_ms ) {
                if( dt > timeout_ms ) {
                    lastError = CU_PRINT_ERR__PORT_IO_TIMEOUT;

                    break;
                }
            }
        }
    }
    while( (sentBytes < cmdBufferOffs) && (CU_PRINT_ERR__NONE == lastError) );

    if( CU_PRINT_ERR__NONE == lastError ) {
        if( clearAfterSend ) {
            clearCmdBuffer();
        }

        return true;
    }

    return false;
}
//--------------------------------------------------------------------

char*
TCU_BasePrinter::allocBuffer( const char* str )
{
    char*   buf = (char*) malloc( strlen(str) + 1 );


    if( NULL != buf ) {
        strcpy( buf, str );
    }

    return buf;
}
//--------------------------------------------------------------------

bool
TCU_BasePrinter::beforePrintJobStart()
{
    lastError = CU_PRINT_ERR__NONE;

    return true;
}
//--------------------------------------------------------------------

bool
TCU_BasePrinter::afterProcessLine( json_object* jLine, int idx )
{
    lastError = CU_PRINT_ERR__NONE;

    return true;
}
//--------------------------------------------------------------------

bool
TCU_BasePrinter::afterPrintJobFinish( bool result )
{
    return result;
}
//--------------------------------------------------------------------


///*******************************************************************
///PRIVATE METHODS OF BASE_PRINTER CLASS
///*******************************************************************

bool
TCU_BasePrinter::applyStyle( json_object* jItem )
{
    lastError = CU_PRINT_ERR__NONE;

    json_object_object_foreach( jItem, prop_id, prop_val ) {
        lastError = CU_PRINT_ERR__NONE;

        if( 0 == strcmp(prop_id, "pAlignment") ) {
            std::string     Value( json_object_get_string(prop_val) );
            const char*     values[3] = { "left", "right", "center" };
            TCU_PropertyVal sAlignment[3] = { pv_Left, pv_Right, pv_Center };
            TCU_PropertyVal vAlignment = pv_Undef;


            for( size_t i = 0; i < sizeof(values)/sizeof(values[0]); i++ ) {
                if( 0 == Value.compare(values[i]) ) {
                    vAlignment = sAlignment[i];
                    break;
                }
            }

            setProperty(propId_Alignment, vAlignment);
        }

        if( 0 == strcmp(prop_id, "pBarcodeHRI") ) {
            std::string     Value( json_object_get_string(prop_val) );
            const char*     values[4] = { "none", "above", "below", "both" };
            TCU_PropertyVal sHriPos[4] = { pv_None, pv_Above, pv_Below, pv_Both };
            TCU_PropertyVal vHriPos = pv_Undef;


            for( size_t i = 0; i < sizeof(values)/sizeof(values[0]); i++ ) {
                if( 0 == Value.compare(values[i]) ) {
                    vHriPos = sHriPos[i];
                    break;
                }
            }

            setProperty(propId_BarCodeHRI, vHriPos );
        }
        if( 0 == strcmp(prop_id, "pBarcodeW") ) {
            setProperty(propId_BarCodeWidth, pv_None, json_object_get_int(prop_val) );
        }
        if( 0 == strcmp(prop_id, "pBarcodeH") ) {
            setProperty(propId_BarCodeHeight, pv_None, json_object_get_int(prop_val) );
        }

        if( 0 == strcmp(prop_id, "pCharSize") ) {
            std::string     Value( json_object_get_string(prop_val) );
            const char*     values[4] = { "std_size", "dbl_size", "dbl_height", "dbl_width" };
            TCU_PropertyVal sChrSz[4] = { pv_StdSize, pv_DblSize, pv_DblHeight, pv_DblWidth };
            TCU_PropertyVal vChrSz = pv_Undef;


            for( size_t i = 0; i < sizeof(values)/sizeof(values[0]); i++ ) {
                if( 0 == Value.compare(values[i]) ) {
                    vChrSz = sChrSz[i];
                    break;
                }
            }

            setProperty(propId_CharacterSize, vChrSz );
        }


        if( CU_PRINT_ERR__NONE != lastError ) {
            break;
        }
    }

    return ( CU_PRINT_ERR__NONE == lastError )? true: false;
}
//--------------------------------------------------------------------

//--------------------------------------------------------------------
//--------------------------------------------------------------------
