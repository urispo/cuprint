#include "cu_print.h"
#include "utils.h"

#define PS_VIRTUAL__VERSION_STRING  "0.0"

///*******************************************************************
///PUBLIC METHODS OF VIRTUAL_PRINTER CLASS
///*******************************************************************

TCU_PsVirtual::TCU_PsVirtual( const char* file, const char* charset ) :
TCU_BasePrinter( NULL )
{
    if( NULL != charset ) {
        if( ((iconv_t)(-1)) == (hconv = iconv_open(charset, ifceCS)) ) {
            lastError = CU_PRINT_ERR__CREATE_ENC_HANDLE;

            return;
        }
    }

    modelId = CU_PRINT_MODEL_ID__VIRTUAL;

    Device.open( file, std::fstream::in | std::fstream::out | std::fstream::trunc );

    if( Device.fail() ) {
        lastError = CU_PRINT_ERR__PORT_OPEN;
    }
    else DevFileName = file;
}
//--------------------------------------------------------------------

TCU_PsVirtual::~TCU_PsVirtual()
{
    if( Device.is_open() ) {
        Device.close();
    }
}
//--------------------------------------------------------------------

bool
TCU_PsVirtual::init( json_object* jSettings )
{
    lastError = CU_PRINT_ERR__NONE;

    pAlignment = pv_Left;

    if( ! Device.is_open() ) {
        lastError = CU_PRINT_ERR__PORT_OPEN;

        return false;
    }

    return true;
}
//--------------------------------------------------------------------

bool
TCU_PsVirtual::cancelPrintJob()
{
    lastError = CU_PRINT_ERR__NONE;

    return true;
}
//--------------------------------------------------------------------

bool
TCU_PsVirtual::printText( const char* text )
{
    if( ! Device.is_open() ) {
        Device.open( DevFileName.c_str(), std::fstream::in | std::fstream::out | std::fstream::trunc );
    }

    std::wstring    WText;
    std::string     Line;
    bool            loopEnable = true;


    lastError = CU_PRINT_ERR__NONE;

    if( ((size_t) -1) == mbStr2wcStr(text, WText) ) {
        lastError = CU_PRINT_ERR__MBS_TO_WCS;

        return false;
    }

    do {
        if( WText.length() <= ((size_t) ptColumns) ) {
            switch( pAlignment ) {
                case pv_Right:
                    WText.insert( 0, ptColumns - WText.size(), L' ' );
                    break;

                case pv_Center: {
                    for( int i = 0; WText.size() < ((size_t) ptColumns); i++ ) {
                        if( i % 2 ) {
                            WText.append( 1, L' ' );
                        }
                        else WText.insert( 0, 1, L' ' );
                    }

                    break;
                }

                default: break;
            }

            if( ((size_t) -1) == wcStr2mbStr(WText.c_str(), Line) ) {
                lastError = CU_PRINT_ERR__WCS_TO_MBS;

                return false;
            }

            loopEnable = false;
        }
        else {
            if( ((size_t) -1) == wcStr2mbStr(WText.substr(0, ptColumns).c_str(), Line) ) {
                lastError = CU_PRINT_ERR__WCS_TO_MBS;

                return false;
            }
            WText.erase( 0, ptColumns );
        }

        //Device.write( Line.c_str(), Line.length() );
        if( ! Line.empty() ) {
            Device << Line << std::endl;
        }
    }
    while( loopEnable );


    return true;
}
//--------------------------------------------------------------------

bool
TCU_PsVirtual::sendRawDataBin( uint8_t* buffer, size_t bytesCount )
{
    lastError = CU_PRINT_ERR__UNSUPPORTED_FEATURE;

    return false;
}
//--------------------------------------------------------------------

bool
TCU_PsVirtual::feedPaper( TCU_FeedMode fpMode, int units )
{
    if( (units < 0) || (fpMode_Lines != fpMode) ) {
        lastError = CU_PRINT_ERR__UNSUPPORTED_FEATURE;

        return false;
    }
    else lastError = CU_PRINT_ERR__NONE;

    for( int i = 0; i < units; i++ ) {
        Device << std::endl;

        if( ! Device.good() ) {
            lastError = CU_PRINT_ERR__PORT_WRITE;

            return false;
        }
    }

    return true;
}

bool
TCU_PsVirtual::cutPaper( TCU_CutterType coType, int adv )
{
    std::string CutMark( ptColumns, '-' );


    lastError = CU_PRINT_ERR__NONE;

    Device << CutMark << std::endl;

    return Device.good();
}
//--------------------------------------------------------------------

bool
TCU_PsVirtual::printBarCode( TCU_BarcodeType bcType, const char* bcData, ... )
{
    if( ! Device.is_open() ) {
        Device.open( DevFileName.c_str(), std::fstream::in | std::fstream::out | std::fstream::trunc );
    }
    Device << bcData << std::endl;

    return Device.good();
}
//--------------------------------------------------------------------
//--------------------------------------------------------------------
//--------------------------------------------------------------------


///*******************************************************************
///PROTECTED METHODS OF VIRTUAL_PRINTER CLASS
///*******************************************************************
//--------------------------------------------------------------------
//--------------------------------------------------------------------
//--------------------------------------------------------------------



///*******************************************************************
///PRIVATE METHODS OF VIRTUAL_PRINTER CLASS
///*******************************************************************

bool
TCU_PsVirtual::beforePrintJobStart()
{
    if( Device.is_open() ) {
        Device.close();
    }
    Device.open( DevFileName.c_str(), std::fstream::in | std::fstream::out | std::fstream::trunc );

    lastError = CU_PRINT_ERR__NONE;

    if( Device.fail() ) {
        lastError = CU_PRINT_ERR__PORT_OPEN;

        return false;
    }

    return true;
}
//--------------------------------------------------------------------

bool
TCU_PsVirtual::afterPrintJobFinish( bool result )
{
    if( result ) {
        Device.close();
    }

    return result;
}
//--------------------------------------------------------------------

const char*
TCU_PsVirtual::getVersionStr()
{
    return PS_VIRTUAL__VERSION_STRING;
}
//--------------------------------------------------------------------

bool
TCU_PsVirtual::updateState()
{
    lastError = CU_PRINT_ERR__NONE;

    State.online = true;
    State.errorsDetected = false;
    State.unrecoverableError = false;
    State.recoverableError = false;
    State.paperEndDetected = false;
    State.paperNearEndDetected = false;
    State.coverIsOpened = false;

    return true;
}
//--------------------------------------------------------------------

bool
TCU_PsVirtual::finalizePrintJob( json_object* jPJob )
{
    lastError = CU_PRINT_ERR__NONE;

    return true;
}
//--------------------------------------------------------------------

bool
TCU_PsVirtual::putCmd_Print()
{
    Device << std::endl;

    return Device.good();
}
//--------------------------------------------------------------------

bool
TCU_PsVirtual::putCmd_SetAlignment( TCU_PropertyVal propVal )
{
    lastError = CU_PRINT_ERR__NONE;

    switch( propVal ) {
        case pv_Left:
        case pv_Center:
        case pv_Right:
            pAlignment = propVal;
            break;

        default: lastError = CU_PRINT_ERR__INVALID_PROP_VAL; break;
    }


    return ( CU_PRINT_ERR__NONE == lastError );
}
//--------------------------------------------------------------------

bool
TCU_PsVirtual::putCmd_SetBarcodeWidth( int widthCode )
{
    lastError = CU_PRINT_ERR__NONE;

    return true;
}
//--------------------------------------------------------------------

bool
TCU_PsVirtual::putCmd_SetBarcodeHeight( int heightDots )
{
    lastError = CU_PRINT_ERR__NONE;

    return true;
}
//--------------------------------------------------------------------

bool
TCU_PsVirtual::putCmd_SetBarcodeHriPos( TCU_PropertyVal propVal )
{
    lastError = CU_PRINT_ERR__NONE;

    return true;
}
//--------------------------------------------------------------------
//--------------------------------------------------------------------
//--------------------------------------------------------------------

