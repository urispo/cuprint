#include "cu_print.h"


#define OKI_TP__VERSION_STRING  "0.2"


///*******************************************************************
///PUBLIC METHODS OF OKI_THERMAL_PRINTER CLASS
///*******************************************************************


TCU_MitsuThermal::TCU_MitsuThermal( const char* port, const char* model ) :
TCU_BasePrinter( port )
{
    if( 0 == strcmp("RP890", model) ) {
        modelId = CU_PRINT_MODEL_ID__PT330;
    }
    else {
        lastError = CU_PRINT_ERR__INVALID_MODEL;

        return;
    }

    if( ((iconv_t)(-1)) == (hconv = iconv_open("CP866//TRANSLIT", ifceCS)) ) {
        lastError = CU_PRINT_ERR__CREATE_ENC_HANDLE;
        return;
    }
}
//--------------------------------------------------------------------

TCU_MitsuThermal::~TCU_MitsuThermal()
{
}
//--------------------------------------------------------------------

bool
TCU_MitsuThermal::init( json_object* jConfig )
{
    if( CU_PRINT_MODEL_ID__UNDEF == modelId ) {
        lastError = CU_PRINT_ERR__INVALID_MODEL;

        return false;
    }

    DefaultPortSettings.baudrate = 115200;
    DefaultPortSettings.databits = 8;
    DefaultPortSettings.stopbits = 1;
    DefaultPortSettings.parity   = SP_PARITY_NONE;
    DefaultPortSettings.flowcontrol  = SP_FLOWCONTROL_NONE;

    if( ! configPort(getPortSettings(jConfig)) ) {
        lastError = CU_PRINT_ERR__PORT_CONFIG;

        return false;
    }

    clearCmdBuffer();

    if( ! fillCmdBuffer(2, CH__ESC, '@') ) {
        return false;
    }

    json_object*        jValue = NULL;


    if( json_object_object_get_ex(jConfig, "leftMargin", &jValue) ) {
        int n = json_object_get_int( jValue );
        storedLeftMargin = n;

        if( ! fillCmdBuffer(4, CH__GS, 'L', n, (n >> 8)) ) {
            return false;
        }
    } else
        storedLeftMargin = 0;

    if( json_object_object_get_ex(jConfig, "printAreaWidth", &jValue) ) {
        int n = json_object_get_int( jValue );
        storedPrintAreaWidth = n;

        if( ! fillCmdBuffer(4, CH__GS, 'W', n, (n >> 8)) ) {
            return false;
        }
    } else
        storedPrintAreaWidth = 0;

    /// select character code table: PC866
    if( ! fillCmdBuffer(3, CH__ESC, 't', 17) ) {
        return false;
    }

    /// select character fonts
    if( ! fillCmdBuffer(3, CH__ESC, 'M', 0) ) {
        return false;
    }

    /// define user-defined characters
    char    data[3][40] = {
            {
                12,
                0x00, 0x18, 0x00,
                0x00, 0x3c, 0x00,
                0x00, 0x7e, 0x00,
                0x00, 0xff, 0x00,
                0x01, 0xff, 0x80,
                0x03, 0xff, 0xc0,
                0x07, 0xff, 0xe0,
                0x0f, 0x7e, 0xf0,
                0x1e, 0x7e, 0x78,
                0x3c, 0x7e, 0x3c,
                0x78, 0x7e, 0x1e,
                0xf0, 0x7e, 0x0f
            },
            {
                12,
                0x00, 0x7e, 0x00,
                0x00, 0x7e, 0x00,
                0x00, 0x7e, 0x00,
                0x00, 0x7e, 0x00,
                0x00, 0x7e, 0x00,
                0x00, 0x7e, 0x00,
                0x00, 0x7e, 0x00,
                0x00, 0x7e, 0x00,
                0x00, 0x7e, 0x00,
                0x00, 0x7e, 0x00,
                0x00, 0x7e, 0x00,
                0x00, 0x7e, 0x00
            },
            {
                12,
                0xf0, 0x7e, 0x0f,
                0x78, 0x7e, 0x1e,
                0x3c, 0x7e, 0x3c,
                0x1e, 0x7e, 0x78,
                0x0f, 0x7e, 0xf0,
                0x07, 0xff, 0xe0,
                0x03, 0xff, 0xc0,
                0x01, 0xff, 0x80,
                0x00, 0xff, 0x00,
                0x00, 0x7e, 0x00,
                0x00, 0x3c, 0x00,
                0x00, 0x18, 0x00
            }
    };


    if( ! fillCmdBuffer(5, CH__ESC, '&', 3, '<', '>') ) {
        return false;
    }
    for( int i = 0; i < 3; i++ ) {
        if( ! fillCmdBuffer(37, data[i]) ) {
            return false;
        }
    }

    return sendCmdBuffer( 10000 );
}
//--------------------------------------------------------------------

bool
TCU_MitsuThermal::cancelPrintJob()
{
/*    uint8_t d[] = { 1, 3, 20, 1, 6, 2, 8 };


    if( ! fillCmdBuffer(3, CH__DLE, CH__DC4, 0x08) ) {
        return false;
    }
    if( !fillCmdBuffer(7, d) ) {
        return false;
    }

    return sendCmdBuffer( 5000 ); */

    // Since Mitsu has no "Clear buffer" command, we re-init the printer (it clears buffer)

    clearCmdBuffer();

    if( ! fillCmdBuffer(2, CH__ESC, '@') ) {
        return false;
    }


    if( storedLeftMargin != 0 ) {
        if( ! fillCmdBuffer(4, CH__GS, 'L', storedLeftMargin, (storedLeftMargin >> 8)) ) {
            return false;
        }
    }

    if( storedPrintAreaWidth != 0 ) {
        if( ! fillCmdBuffer(4, CH__GS, 'W', storedPrintAreaWidth, (storedPrintAreaWidth >> 8)) ) {
            return false;
        }
    }

    /// select character code table: PC866
    if( ! fillCmdBuffer(3, CH__ESC, 't', 17) ) {
        return false;
    }

    /// select character fonts
    if( ! fillCmdBuffer(3, CH__ESC, 'M', 0) ) {
        return false;
    }

    /// define user-defined characters
    char    data[3][40] = {
            {
                12,
                0x00, 0x18, 0x00,
                0x00, 0x3c, 0x00,
                0x00, 0x7e, 0x00,
                0x00, 0xff, 0x00,
                0x01, 0xff, 0x80,
                0x03, 0xff, 0xc0,
                0x07, 0xff, 0xe0,
                0x0f, 0x7e, 0xf0,
                0x1e, 0x7e, 0x78,
                0x3c, 0x7e, 0x3c,
                0x78, 0x7e, 0x1e,
                0xf0, 0x7e, 0x0f
            },
            {
                12,
                0x00, 0x7e, 0x00,
                0x00, 0x7e, 0x00,
                0x00, 0x7e, 0x00,
                0x00, 0x7e, 0x00,
                0x00, 0x7e, 0x00,
                0x00, 0x7e, 0x00,
                0x00, 0x7e, 0x00,
                0x00, 0x7e, 0x00,
                0x00, 0x7e, 0x00,
                0x00, 0x7e, 0x00,
                0x00, 0x7e, 0x00,
                0x00, 0x7e, 0x00
            },
            {
                12,
                0xf0, 0x7e, 0x0f,
                0x78, 0x7e, 0x1e,
                0x3c, 0x7e, 0x3c,
                0x1e, 0x7e, 0x78,
                0x0f, 0x7e, 0xf0,
                0x07, 0xff, 0xe0,
                0x03, 0xff, 0xc0,
                0x01, 0xff, 0x80,
                0x00, 0xff, 0x00,
                0x00, 0x7e, 0x00,
                0x00, 0x3c, 0x00,
                0x00, 0x18, 0x00
            }
    };


    if( ! fillCmdBuffer(5, CH__ESC, '&', 3, '<', '>') ) {
        return false;
    }
    for( int i = 0; i < 3; i++ ) {
        if( ! fillCmdBuffer(37, data[i]) ) {
            return false;
        }
    }

    return sendCmdBuffer( 10000 );
}
//--------------------------------------------------------------------

bool
TCU_MitsuThermal::sendRawDataBin( uint8_t* buffer, size_t bytesCount )
{
    return sendBytesToSP( buffer, bytesCount, 0 );
}
//--------------------------------------------------------------------
/*
bool
TCU_OkiThermal::feedPaper( TCU_FeedMode fpMode, int units )
{
    if( units < 0 ) {
        lastError = CU_PRINT_ERR__UNSUPPORTED_FEATURE;

        return false;
    }

    clearCmdBuffer();

    switch( fpMode ) {
        case fpMode_Lines:  fillCmdBuffer( 3, CH__ESC, 'd', units ); break;
        case fpMode_Dots:   fillCmdBuffer( 3, CH__ESC, 'J', units ); break;

        default: lastError = CU_PRINT_ERR__ARG_IS_OUT_OF_RANGE;  return false;
    }

    return sendCmdBuffer( 1000 );
}
//--------------------------------------------------------------------

bool
TCU_OkiThermal::cutPaper( TCU_CutterType coType, int adv )
{
    int type;


    clearCmdBuffer();

    switch( coType ) {
        case coType_Full:       type = ( adv < 0 )? 0: 65;  break;
        case coType_Partial:    type = ( adv < 0 )? 1: 66;  break;

        default:
            lastError = CU_PRINT_ERR__ARG_IS_OUT_OF_RANGE;  return false;
    }

    if( adv < 0 ) {
        fillCmdBuffer( 3, CH__GS, 'V', type );
    }
    else fillCmdBuffer( 4, CH__GS, 'V', type, adv );

    if( CU_PRINT_ERR__NONE != lastError ) {
        return false;
    }

    if( ! sendCmdBuffer(1000) ) {
        clearCmdBuffer();

        return false;
    }

    return true;
}
//--------------------------------------------------------------------
*/
bool
TCU_MitsuThermal::printBarCode( TCU_BarcodeType bcType, const char* bcData, ... )
{
    int codeLen = strlen( bcData );


    clearCmdBuffer();

    switch( bcType ) {
        case bcType_Code128:
            // select character font for HRI characters: Font B
            if( ! fillCmdBuffer(3, CH__GS, 'f', 1) ) {
                return false;
            }
            if( ! fillCmdBuffer(6, CH__GS, 'k', 73, codeLen + 2, 0x7B, 0x42) ) {
                return false;
            }
            if( ! fillCmdBuffer(codeLen, bcData) ) {
                return false;
            }

            break;

        default:
            lastError = CU_PRINT_ERR__INVALID_BC_TYPE;

            return false;
    }

    if( ! sendCmdBuffer(10000) ) {
        clearCmdBuffer();

        return false;
    }

    return true;
}
//--------------------------------------------------------------------
//--------------------------------------------------------------------



///*******************************************************************
///PROTECTED METHODS OF OKI_THERMAL_PRINTER CLASS
///*******************************************************************


///*******************************************************************
///PRIVATE METHODS OF OKI_THERMAL_PRINTER CLASS
///*******************************************************************


const char*
TCU_MitsuThermal::getVersionStr()
{
    return OKI_TP__VERSION_STRING;
}
//--------------------------------------------------------------------

bool
TCU_MitsuThermal::beforePrintJobStart( void )
{
    lastError = CU_PRINT_ERR__NONE;

    if( SP_OK != sp_flush(SP, SP_BUF_INPUT) ) {
        lastError = CU_PRINT_ERR__PORT_FLUSH_DATA;
        return false;
    }

    return true;
}
//--------------------------------------------------------------------

bool
TCU_MitsuThermal::finalizePrintJob( json_object* jPJob )
{
    int pjProgress = json_object_get_int( json_object_object_get(jPJob, "pjProgress") );
/*
    char    idStr[8];
    char    wnd[2] = { 0, 0 };
    uint8_t asb[4], offs = 0;


    snprintf( idStr, sizeof(idStr), "%.4X", 0xffff & rand() );

    if( 11 == fillCmdBuffer(11, CH__GS, '(', 'H', 6, 0, 48, 48, idStr[0], idStr[1], idStr[2], idStr[3]) ) {
        if( ! sendCmdBuffer(1000) ) {
            return false;
        }
    }
    else return false;

    int pjProgress = json_object_get_int( json_object_object_get(jPJob, "pjProgress") );
    int ioResult;


    do {
        if( (ioResult = sp_input_waiting(SP)) < 0 ) {
            lastError = CU_PRINT_ERR__PORT_READ;

            break;
        }
        if( 0 == ioResult ) continue;

        if( (ioResult = sp_nonblocking_read(SP, wnd, 1)) < 0 ) {
            lastError = CU_PRINT_ERR__PORT_READ;

            break;
        }

        if( ioResult ) {
            if( 0x3722 == (*(uint16_t*) wnd) ) {
                char    buf[8];


                offs = 0;
                ioResult = sp_blocking_read( SP, buf, 5, 1000 );

                if( ioResult < 5 ) {
                    lastError = ( ioResult < 0 )? CU_PRINT_ERR__PORT_READ: CU_PRINT_ERR__PORT_IO_TIMEOUT;
                }

                if( strncmp(idStr, buf, 4) ) {
                    pjProgress = atoi( buf );
                }
                else break;
            }
            else {
                wnd[1] = wnd[0];

                if( offs < sizeof(asb) ) {
                    asb[offs++] = wnd[0];
                }

                if( sizeof(asb) == offs ) {
                    if( (0x10 != (asb[0] & 0x93)) || (0x00 != (asb[1] & 0x90)) || \
                        (0x00 != (asb[2] & 0xF0)) || (0x0F != (asb[3] & 0x9F)) ) {
                        lastError = CU_PRINT_ERR__INVALID_ASB_DATA;

                        break;
                    }

                    offs = 0;

                    if( asb[0] & 0x08 ) {
                    // offline state
                        if( ! this->cancelPrintJob() ) {
                            break;
                        }

                        if( asb[0] & 0x20 ) {
                            lastError = CU_PRINT_ERR__COVER_IS_OPENED;
                        }
                        else
                        if( asb[2] & 0x0C ) {
                            lastError = CU_PRINT_ERR__PAPER_END;
                        }
                        else lastError = CU_PRINT_ERR__OFFLINE_STATE;

                        break;
                    }
                }
            }
        }

        usleep( 10000 );
    }
    while( true );
*/
//    sleep( 0.035 * (storeIdx - pjProgress) );
    if ( this->updateState() ) {
        if ( ! State.online )
            lastError = CU_PRINT_ERR__OFFLINE_STATE;
        if ( State.coverIsOpened )
            lastError = CU_PRINT_ERR__COVER_IS_OPENED;
        if ( State.paperEndDetected )
            lastError = CU_PRINT_ERR__PAPER_END;
    }
    else
        lastError = CU_PRINT_ERR__INVALID_STATUS_DATA;

    if ( CU_PRINT_ERR__NONE != lastError )
        this->cancelPrintJob();

    pjProgress = storeIdx;
    json_object_object_del( jPJob, "pjProgress" );
    json_object_object_add( jPJob, "pjProgress", json_object_new_int(pjProgress) );


    return ( CU_PRINT_ERR__NONE == lastError );
}
//--------------------------------------------------------------------

bool
TCU_MitsuThermal::updateState()
{
    uint8_t status[4];
    uint8_t mask0 = 0x81, mask1 = 0x12;
    char    buf[32];


    lastError = CU_PRINT_ERR__NONE;
    State.RawBytes = "";

    for( size_t  n = 0; n < sizeof(status); n++ ) {
        int result = getRealTimeStatusByte( n + 1 );

        if( result < 0 ) {
            return false;
        }
        else status[n] = (uint8_t) result;

//        sprintf( buf, "%.2hhX", status[n] );
//        State.RawBytes += std::string( buf, 2 );

        if( (mask0 != (~status[n] & mask0)) || (mask1 != (status[n] & mask1)) ) {
            lastError = CU_PRINT_ERR__INVALID_STATUS_DATA;

            return false;
        }
    }

    State.online                = ( status[0] & 0x08 )? false: true;
    State.coverIsOpened         = ( status[1] & 0x04 )? true: false;
    State.errorsDetected        = ( status[1] & 0x40 )? true: false;
    State.recoverableError      = ( status[2] & 0x40 )? true: false;
    State.unrecoverableError    = ( status[2] & 0x20 )? true: false;
    State.paperNearEndDetected  = ( status[3] & 0x0C )? true: false;
    State.paperEndDetected      = ( status[3] & 0x60 )? true: false;
    State.lowBatteryVoltage     = false;
    State.overheatDetected      = false;


    if( State.online ) {
        sprintf( buf, "%d", getCounterValue(20) );
        State.RawBytes = buf;
    }


    return true;
}
//--------------------------------------------------------------------

bool
TCU_MitsuThermal::afterProcessLine( json_object* jLine, int idx )
{
//    lastError = CU_PRINT_ERR__NONE;

// Following code manage printer to twitch so will comment it
/*    if( this->updateState() ) {
        if( ! State.online ) {
            lastError = CU_PRINT_ERR__OFFLINE_STATE;

            return false;
        }
        else {
            storeIdx = idx;
            return true;
            }
    }
    else return false;
*/

    storeIdx = idx;
    return true;

/*    if( json_object_is_type(jLine, json_type_string) ) {
        char    buf[8];


        snprintf( buf, sizeof(buf), "%.4d", idx );

        if( 11 != fillCmdBuffer(11, CH__GS, '(', 'H', 6, 0, 48, 48, buf[0], buf[1], buf[2], buf[3]) ) {
            clearCmdBuffer();

            return false;
        }

        return sendCmdBuffer(1000);
    }

    return true;
*/
}
//--------------------------------------------------------------------

bool
TCU_MitsuThermal::putCmd_Print()
{
    return fillCmdBuffer( 1, '\n' );
}
//--------------------------------------------------------------------

bool
TCU_MitsuThermal::putCmd_SetAlignment( TCU_PropertyVal propVal )
{
    int alProp;


    switch( propVal ) {
        case pv_Left:   alProp = 0; break;
        case pv_Center: alProp = 1; break;
        case pv_Right:  alProp = 2; break;

        default:
            lastError = CU_PRINT_ERR__INVALID_PROP_VAL;

            return false;
    }


    return fillCmdBuffer( 3, CH__ESC, 'a', alProp );
}
//--------------------------------------------------------------------
bool
TCU_MitsuThermal::putCmd_SetBarcodeWidth( int widthCode )
{
    if( (widthCode < 1) || (widthCode > 6) ) {
        widthCode = 3;
    }

    return fillCmdBuffer( 3, CH__GS, 'w', widthCode );
}
//--------------------------------------------------------------------

bool
TCU_MitsuThermal::putCmd_SetBarcodeHeight( int heightDots )
{
    if( (heightDots < 1) || (heightDots > 255) ) {
        heightDots = 162;
    }

    return fillCmdBuffer( 3, CH__GS, 'h', heightDots );
}
//--------------------------------------------------------------------

bool
TCU_MitsuThermal::putCmd_SetBarcodeHriPos( TCU_PropertyVal propVal )
{
    int hriProp;


    switch( propVal ) {
        case pv_None:   hriProp = 0; break;
        case pv_Above:  hriProp = 1; break;
        case pv_Below:  hriProp = 2; break;
        case pv_Both:   hriProp = 3; break;

        default:
            lastError = CU_PRINT_ERR__INVALID_PROP_VAL;

            return false;
    }

    return fillCmdBuffer( 3, CH__GS, 'H', hriProp );
}

//--------------------------------------------------------------------

bool
TCU_MitsuThermal::putCmd_SetCharacterSize( TCU_PropertyVal propVal )
{
    int chrSize;


    switch( propVal ) {
        case pv_StdSize:    chrSize = 0x00; break;
        case pv_DblSize:    chrSize = 0x11; break;
        case pv_DblHeight:  chrSize = 0x01; break;
        case pv_DblWidth:   chrSize = 0x10; break;

        default:
            lastError = CU_PRINT_ERR__INVALID_PROP_VAL;

            return false;
    }

    return fillCmdBuffer( 3, CH__GS, '!', chrSize );
}
//--------------------------------------------------------------------

int
TCU_MitsuThermal::getRealTimeStatusByte( unsigned char n )
{
    lastError = CU_PRINT_ERR__NONE;

    if( (n < 1) || (n > 4) ) {
        lastError = CU_PRINT_ERR__ARG_IS_OUT_OF_RANGE;
        return (-1);
    }

    char    txBuf[] = { CH__DLE, CH__EOT, n };
    char    rxByte = 0;
    int     ioResult;


    if( SP_OK != sp_flush(SP, SP_BUF_INPUT) ) {
        lastError = CU_PRINT_ERR__PORT_FLUSH_DATA;
        return (-1);
    }

    ioResult = sp_blocking_write( SP, txBuf, sizeof(txBuf), 1000 );
    if( ioResult < (int) sizeof(txBuf) ) {
        lastError = ( ioResult < 0 )? CU_PRINT_ERR__PORT_WRITE: CU_PRINT_ERR__PORT_IO_TIMEOUT;
        return (-1);
    }

    ioResult = sp_blocking_read( SP, &rxByte, 1, 2000 );
    if( ioResult < 1 ) {
        lastError = ( ioResult < 0 )? CU_PRINT_ERR__PORT_READ: CU_PRINT_ERR__PORT_IO_TIMEOUT;
        return (-1);
    }


    return rxByte;
}
//--------------------------------------------------------------------

int
TCU_MitsuThermal::getCounterValue( int counterId )
{
    char    txBuf[] = { CH__GS, 'g', '2', 0, (char) (counterId % 256), 0 };
    int     ioResult;


    lastError = CU_PRINT_ERR__NONE;

    if( SP_OK != sp_flush(SP, SP_BUF_INPUT) ) {
        lastError = CU_PRINT_ERR__PORT_FLUSH_DATA;
        return (-1);
    }

    ioResult = sp_blocking_write( SP, txBuf, sizeof(txBuf), 1000 );
    if( ioResult < (int) sizeof(txBuf) ) {
        lastError = ( ioResult < 0 )? CU_PRINT_ERR__PORT_WRITE: CU_PRINT_ERR__PORT_IO_TIMEOUT;
        return (-1);
    }

    char    rxBuf[64];
    size_t  offs = 0;


    do {
        ioResult = sp_blocking_read( SP, &rxBuf[offs], 1, 2000 );
        if( ioResult < 1 ) {
            lastError = ( ioResult < 0 )? CU_PRINT_ERR__PORT_READ: CU_PRINT_ERR__PORT_IO_TIMEOUT;

            return (-1);
        }

        if( '\0' == rxBuf[offs] ) {
            return atoi( &rxBuf[1] );
        }

        if( '_' == rxBuf[0] ) {
            offs++;
        }
    }
    while( offs < sizeof(rxBuf) );

    lastError = CU_PRINT_ERR__BUFFER_SPACE;

    return (-1);
}

//--------------------------------------------------------------------
char*
TCU_MitsuThermal::allocBuffer( const char* str )
{
    if( NULL == str ) {
        return NULL;
    }

    char*   buf = (char*) malloc( strlen(str) + 1 );


    if( NULL != buf ) {
        strcpy( buf, str );

        size_t  i = 0, n;


        while( (n = strlen(&buf[i])) >= 2) {
            if( (buf[i] == 0xD0) && (buf[i+1] == 0x86) ) {
                buf[i] = 'I';
                memmove( &buf[i+1], &buf[i+2], n - 1 );
            }
            else
            if( (buf[i] == 0xD1) && (buf[i+1] == 0x96) ) {
                buf[i] = 'i';
                memmove( &buf[i+1], &buf[i+2], n - 1 );
            }
            else i++;
        }
    }

    return buf;
}
//--------------------------------------------------------------------
