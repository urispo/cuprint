#include "cu_print.h"


#define OKI_TP__VERSION_STRING  "0.2"


///*******************************************************************
///PUBLIC METHODS OF OKI_THERMAL_PRINTER CLASS
///*******************************************************************

TCU_OkiThermal::TCU_OkiThermal( const char* port, const char* model ) :
TCU_BasePrinter( port )
{
    if( 0 == strcmp("PT330", model) ) {
        modelId = CU_PRINT_MODEL_ID__PT330;
    }
    else
    if( 0 == strcmp("PT340", model) ) {
        modelId = CU_PRINT_MODEL_ID__PT340;
    }
    else {
        lastError = CU_PRINT_ERR__INVALID_MODEL;

        return;
    }

    if( ((iconv_t)(-1)) == (hconv = iconv_open("CP866//TRANSLIT", ifceCS)) ) {
        lastError = CU_PRINT_ERR__CREATE_ENC_HANDLE;
        return;
    }
}
//--------------------------------------------------------------------

TCU_OkiThermal::~TCU_OkiThermal()
{
}
//--------------------------------------------------------------------

bool
TCU_OkiThermal::init( json_object* jConfig )
{
    if( CU_PRINT_MODEL_ID__UNDEF == modelId ) {
        lastError = CU_PRINT_ERR__INVALID_MODEL;

        return false;
    }

    DefaultPortSettings.baudrate = 115200;
    DefaultPortSettings.databits = 8;
    DefaultPortSettings.stopbits = 1;
    DefaultPortSettings.parity   = SP_PARITY_NONE;
    DefaultPortSettings.flowcontrol  = SP_FLOWCONTROL_NONE;

    if( ! configPort(getPortSettings(jConfig)) ) {
        lastError = CU_PRINT_ERR__PORT_CONFIG;

        return false;
    }

    clearCmdBuffer();

    if( ! fillCmdBuffer(2, CH__ESC, '@') ) {
        return false;
    }

    json_object*        jValue = NULL;


    if( json_object_object_get_ex(jConfig, "leftMargin", &jValue) ) {
        int n = json_object_get_int( jValue );


        if( ! fillCmdBuffer(4, CH__GS, 'L', n, (n >> 8)) ) {
            return false;
        }
    }
    if( json_object_object_get_ex(jConfig, "printAreaWidth", &jValue) ) {
        int n = json_object_get_int( jValue );


        if( ! fillCmdBuffer(4, CH__GS, 'W', n, (n >> 8)) ) {
            return false;
        }
    }

    /// select character code table: PC866
    if( ! fillCmdBuffer(3, CH__ESC, 't', 17) ) {
        return false;
    }

    /// select character fonts
    if( ! fillCmdBuffer(3, CH__ESC, 'M', 0) ) {
        return false;
    }

    /// define user-defined characters
    char    data[3][40] = {
            {
                12,
                0x00, 0x18, 0x00,
                0x00, 0x3c, 0x00,
                0x00, 0x7e, 0x00,
                0x00, 0xff, 0x00,
                0x01, 0xff, 0x80,
                0x03, 0xff, 0xc0,
                0x07, 0xff, 0xe0,
                0x0f, 0x7e, 0xf0,
                0x1e, 0x7e, 0x78,
                0x3c, 0x7e, 0x3c,
                0x78, 0x7e, 0x1e,
                0xf0, 0x7e, 0x0f
            },
            {
                12,
                0x00, 0x7e, 0x00,
                0x00, 0x7e, 0x00,
                0x00, 0x7e, 0x00,
                0x00, 0x7e, 0x00,
                0x00, 0x7e, 0x00,
                0x00, 0x7e, 0x00,
                0x00, 0x7e, 0x00,
                0x00, 0x7e, 0x00,
                0x00, 0x7e, 0x00,
                0x00, 0x7e, 0x00,
                0x00, 0x7e, 0x00,
                0x00, 0x7e, 0x00
            },
            {
                12,
                0xf0, 0x7e, 0x0f,
                0x78, 0x7e, 0x1e,
                0x3c, 0x7e, 0x3c,
                0x1e, 0x7e, 0x78,
                0x0f, 0x7e, 0xf0,
                0x07, 0xff, 0xe0,
                0x03, 0xff, 0xc0,
                0x01, 0xff, 0x80,
                0x00, 0xff, 0x00,
                0x00, 0x7e, 0x00,
                0x00, 0x3c, 0x00,
                0x00, 0x18, 0x00
            }
    };

    char diacritics[10][37] = {
        { // Ć
            12,
            0x00, 0x00, 0x00,
            0x01, 0xFF, 0xC0,
            0x07, 0xFF, 0xF0,
            0x0E, 0x00, 0x38,
            0x0C, 0x00, 0x18,
            0x2C, 0x00, 0x18,
            0x6C, 0x00, 0x18,
            0xCC, 0x00, 0x18,
            0x8E, 0x00, 0x38,
            0x07, 0x00, 0x70,
            0x03, 0x00, 0x60,
            0x00, 0x00, 0x00
        },
        { // Č
            12,
            0x00, 0x00, 0x00,
            0x01, 0xFF, 0xC0,
            0x07, 0xFF, 0xF0,
            0x8E, 0x00, 0x38,
            0xCC, 0x00, 0x18,
            0x6C, 0x00, 0x18,
            0x6C, 0x00, 0x18,
            0xCC, 0x00, 0x18,
            0x8E, 0x00, 0x38,
            0x07, 0x00, 0x70,
            0x03, 0x00, 0x60,
            0x00, 0x00, 0x00
        },
        { // Ł
            12,
            0x00, 0x0C, 0x00,
            0x00, 0x18, 0x00,
            0x0F, 0xFF, 0xF8,
            0x0F, 0xFF, 0xF8,
            0x00, 0xC0, 0x18,
            0x01, 0x80, 0x18,
            0x03, 0x00, 0x18,
            0x00, 0x00, 0x18,
            0x00, 0x00, 0x18,
            0x00, 0x00, 0x18,
            0x00, 0x00, 0x18,
            0x00, 0x00, 0x00
        },
        { // Ĺ
            12,
            0x00, 0x00, 0x00,
            0x00, 0x00, 0x00,
            0x0F, 0xFF, 0xF8,
            0x0F, 0xFF, 0xF8,
            0x20, 0x00, 0x18,
            0x60, 0x00, 0x18,
            0xC0, 0x00, 0x18,
            0xC0, 0x00, 0x18,
            0x80, 0x00, 0x18,
            0x00, 0x00, 0x18,
            0x00, 0x00, 0x18,
            0x00, 0x00, 0x00
        },
        { // Ń
            12,
            0x00, 0x00, 0x00,
            0x0F, 0xFF, 0xF8,
            0x0F, 0xFF, 0xF8,
            0x07, 0x80, 0x00,
            0x21, 0xE0, 0x00,
            0x60, 0x7C, 0x00,
            0xC0, 0x0F, 0x00,
            0xC0, 0x03, 0xC0,
            0x80, 0x00, 0xF0,
            0x0F, 0xFF, 0xF8,
            0x0F, 0xFF, 0xF8,
            0x00, 0x00, 0x00
        },
        { // Ś
            12,
            0x00, 0x00, 0x00,
            0x01, 0xE0, 0x60,
            0x07, 0xF0, 0x70,
            0x0E, 0x38, 0x38,
            0x2C, 0x18, 0x18,
            0x6C, 0x1C, 0x18,
            0xCC, 0x0C, 0x18,
            0xCC, 0x0E, 0x18,
            0x8E, 0x07, 0x38,
            0x07, 0x03, 0xF0,
            0x03, 0x01, 0xE0,
            0x00, 0x00, 0x00
        },
        { // Š
            12,
            0x00, 0x00, 0x00,
            0x01, 0xE0, 0x60,
            0x07, 0xF0, 0x70,
            0x8E, 0x38, 0x38,
            0xCC, 0x18, 0x18,
            0x6C, 0x1C, 0x18,
            0x6C, 0x0C, 0x18,
            0xCC, 0x0E, 0x18,
            0x8E, 0x07, 0x38,
            0x07, 0x03, 0xF0,
            0x03, 0x01, 0xE0,
            0x00, 0x00, 0x00
        },
        { // Ŭ
            12,
            0x00, 0x00, 0x00,
            0x0F, 0xFF, 0xC0,
            0x0F, 0xFF, 0xF0,
            0xE0, 0x00, 0x38,
            0xF0, 0x00, 0x18,
            0x30, 0x00, 0x18,
            0x30, 0x00, 0x18,
            0xF0, 0x00, 0x18,
            0xE0, 0x00, 0x38,
            0x0F, 0xFF, 0xF0,
            0x0F, 0xFF, 0xE0,
            0x00, 0x00, 0x00
        },
        { // Ź
            12,
            0x00, 0x00, 0x00,
            0x0C, 0x00, 0x38,
            0x0C, 0x00, 0xF8,
            0x0C, 0x01, 0xF8,
            0x2C, 0x07, 0x98,
            0x6C, 0x0E, 0x18,
            0xCC, 0x3C, 0x18,
            0xCC, 0xF0, 0x18,
            0x8F, 0xC0, 0x18,
            0x0F, 0x00, 0x18,
            0x0E, 0x00, 0x18,
            0x00, 0x00, 0x00
        },
        { // Ž
            12,
            0x00, 0x00, 0x00,
            0x0C, 0x00, 0x38,
            0x0C, 0x00, 0xF8,
            0x8C, 0x01, 0xF8,
            0xCC, 0x07, 0x98,
            0x6C, 0x0E, 0x18,
            0x6C, 0x3C, 0x18,
            0xCC, 0xF0, 0x18,
            0x8F, 0xC0, 0x18,
            0x0F, 0x00, 0x18,
            0x0E, 0x00, 0x18,
            0x00, 0x00, 0x00
        }
    };


    // Define user-defined chars - route directions
    if( ! fillCmdBuffer(5, CH__ESC, '&', 3, '<', '>') ) {
        return false;
    }
    for( int i = 0; i < 3; i++ ) {
        if( ! fillCmdBuffer(37, data[i]) ) {
            return false;
        }
    }

/*
0	Ć	C4 86	!
1	Č	C4 8C	#
2	Ł	C5 81	$
3	Ĺ	C4 B9	%
4	Ń	C5 83	&
5	Ś	C5 9A	'
6	Š	C5 A0	/
7	Ŭ	C5 AC	;
8	Ź	C5 B9	?
9	Ž	C5 BD	@
*/

    // Define user-defined chars - diacritics
    if( ! fillCmdBuffer(5, CH__ESC, '&', 3, 0x5C, 0x5C) )   return false;   // backslash
    if( ! fillCmdBuffer(37, diacritics[0]) )                return false;

    if( ! fillCmdBuffer(5, CH__ESC, '&', 3, '{', '{') )     return false;
    if( ! fillCmdBuffer(37, diacritics[1]) )                return false;

    if( ! fillCmdBuffer(5, CH__ESC, '&', 3, '$', '$') )     return false;
    if( ! fillCmdBuffer(37, diacritics[2]) )                return false;

    if( ! fillCmdBuffer(5, CH__ESC, '&', 3, '`', '`') )     return false;
    if( ! fillCmdBuffer(37, diacritics[3]) )                return false;

    if( ! fillCmdBuffer(5, CH__ESC, '&', 3, '&', '&') )     return false;
    if( ! fillCmdBuffer(37, diacritics[4]) )                return false;

    if( ! fillCmdBuffer(5, CH__ESC, '&', 3, '^', '^') )     return false;
    if( ! fillCmdBuffer(37, diacritics[5]) )                return false;

    if( ! fillCmdBuffer(5, CH__ESC, '&', 3, '}', '}') )     return false;
    if( ! fillCmdBuffer(37, diacritics[6]) )                return false;

    if( ! fillCmdBuffer(5, CH__ESC, '&', 3, '|', '|') )     return false;
    if( ! fillCmdBuffer(37, diacritics[7]) )                return false;

    if( ! fillCmdBuffer(5, CH__ESC, '&', 3, '~', '~') )     return false;
    if( ! fillCmdBuffer(37, diacritics[8]) )                return false;

    if( ! fillCmdBuffer(5, CH__ESC, '&', 3, '@', '@') )     return false;
    if( ! fillCmdBuffer(37, diacritics[9]) )                return false;

    return sendCmdBuffer( 10000 );
}
//--------------------------------------------------------------------

bool
TCU_OkiThermal::cancelPrintJob()
{
    uint8_t d[] = { 1, 3, 20, 1, 6, 2, 8 };


    if( ! fillCmdBuffer(3, CH__DLE, CH__DC4, 0x08) ) {
        return false;
    }
    if( !fillCmdBuffer(7, d) ) {
        return false;
    }

    return sendCmdBuffer( 5000 );
}
//--------------------------------------------------------------------

bool
TCU_OkiThermal::sendRawDataBin( uint8_t* buffer, size_t bytesCount )
{
    return sendBytesToSP( buffer, bytesCount, 0 );
}
//--------------------------------------------------------------------
/*
bool
TCU_OkiThermal::gge0Paper( TCU_FeedMode fpMode, int units )
{
    if( units < 0 ) {
        lastError = CU_PRINT_ERR__UNSUPPORTED_FEATURE;

        return false;
    }

    clearCmdBuffer();

    switch( fpMode ) {
        case fpMode_Lines:  fillCmdBuffer( 3, CH__ESC, 'd', units ); break;
        case fpMode_Dots:   fillCmdBuffer( 3, CH__ESC, 'J', units ); break;

        default: lastError = CU_PRINT_ERR__ARG_IS_OUT_OF_RANGE;  return false;
    }

    return sendCmdBuffer( 1000 );
}
//--------------------------------------------------------------------

bool
TCU_OkiThermal::cutPaper( TCU_CutterType coType, int adv )
{
    int type;


    clearCmdBuffer();

    switch( coType ) {
        case coType_Full:       type = ( adv < 0 )? 0: 65;  break;
        case coType_Partial:    type = ( adv < 0 )? 1: 66;  break;

        default:
            lastError = CU_PRINT_ERR__ARG_IS_OUT_OF_RANGE;  return false;
    }

    if( adv < 0 ) {
        fillCmdBuffer( 3, CH__GS, 'V', type );
    }
    else fillCmdBuffer( 4, CH__GS, 'V', type, adv );

    if( CU_PRINT_ERR__NONE != lastError ) {
        return false;
    }

    if( ! sendCmdBuffer(1000) ) {
        clearCmdBuffer();

        return false;
    }

    return true;
}
//--------------------------------------------------------------------
*/
bool
TCU_OkiThermal::printBarCode( TCU_BarcodeType bcType, const char* bcData, ... )
{
    va_list ap;
    int codeLen = strlen( bcData );


    clearCmdBuffer();
    va_start( ap, bcData );

    switch( bcType ) {
        case bcType_Code128:
            // select character font for HRI characters: Font B
            if( ! fillCmdBuffer(3, CH__GS, 'f', 1) ) {
                return false;
            }
            if( ! fillCmdBuffer(6, CH__GS, 'k', 73, codeLen + 2, 0x7B, 0x42) ) {
                return false;
            }
            if( ! fillCmdBuffer(codeLen, bcData) ) {
                return false;
            }

            break;

        case bcType_QR: {
            int     qrSize = va_arg( ap, int );
            int     qrEccl = va_arg( ap, int );


            if( (qrSize < 1) || (qrSize > 16) || (qrEccl < 48) || (qrEccl > 51) ) {
                lastError = CU_PRINT_ERR__INVALID_PROP_VAL;

                break;
            }
            if( codeLen > 448 ) {
                lastError = CU_PRINT_ERR__INVALID_PROP_VAL;

                break;
            }

            if( codeLen <= 0 ) {
                lastError = CU_PRINT_ERR__INVALID_PROP_VAL;

                break;
            }

            if( ! fillCmdBuffer(9, 0x1D, 0x28, 0x6B, 0x04, 0x00, 0x31, 0x41, 0x32, 0x00) ) {
                break;  // model of QR-code (here is 50)
            }
            if( ! fillCmdBuffer(8, 0x1D, 0x28, 0x6B, 0x03, 0x00, 0x31, 0x43, qrSize) ) {
                break;  // module size (01..16)
            }
            if( ! fillCmdBuffer(8, 0x1D, 0x28, 0x6B, 0x03, 0x00, 0x31, 0x45, qrEccl) ) {
                break;  // error level (48..51)
            }
            if( ! fillCmdBuffer(8, 0x1D, 0x28, 0x6B, (codeLen + 3), (codeLen + 3) >> 8, 0x31, 0x50, 0x30) ) {
                break;  // save data header
            }
            if( ! fillCmdBuffer(codeLen, bcData) ) {
                break;  // save data
            }
            if( ! fillCmdBuffer(8, 0x1D, 0x28, 0x6B, 0x03, 0x00, 0x31, 0x51, 0x30) ) {
                break;  // print QR-code
            }

            break;
        }

       case bcType_PDF417: {
            int     pdfModuleWidth = va_arg( ap, int );
            int     pdfHeight = va_arg( ap, int );
            int     pdfEccl = va_arg( ap, int );    // only m=48 implemented


            if( (pdfModuleWidth < 1) || (pdfModuleWidth > 8) || (pdfHeight < 1) || (pdfHeight > 8) || (pdfEccl < 48) || (pdfEccl > 56) ) {
                lastError = CU_PRINT_ERR__INVALID_PROP_VAL;

                break;
            }
            if( (codeLen > 65535) || (codeLen < 4) ) {
                lastError = CU_PRINT_ERR__INVALID_PROP_VAL;

                break;
            }

            if( ! fillCmdBuffer(8, 0x1D, 0x28, 0x6B, 0x03, 0x00, 0x30, 0x41, 0x00) ) {
                break;  // Setting of columns (now is auto)
            }
            if( ! fillCmdBuffer(8, 0x1D, 0x28, 0x6B, 0x03, 0x00, 0x30, 0x42, 0x00) ) {
                break;  // Setting of lows (now is auto)
            }
            if( ! fillCmdBuffer(8, 0x1D, 0x28, 0x6B, 0x03, 0x00, 0x30, 0x43, pdfModuleWidth) ) {
                break;  // Setting of module width
            }
            if( ! fillCmdBuffer(8, 0x1D, 0x28, 0x6B, 0x03, 0x00, 0x30, 0x44, pdfHeight) ) {
                break;  // Setting of module height
            }
            if( ! fillCmdBuffer(9, 0x1D, 0x28, 0x6B, 0x04, 0x00, 0x30, 0x45, 0x30, pdfEccl) ) {
                break;  // Setting of error level
            }
            if( ! fillCmdBuffer(8, 0x1D, 0x28, 0x6B, (codeLen + 3), (codeLen + 3) >> 8, 0x30, 0x50, 0x30) ) {
                break;  // save data header
            }
            if( ! fillCmdBuffer(codeLen, bcData) ) {
                break;  // save data
            }
            if( ! fillCmdBuffer(8, 0x1D, 0x28, 0x6B, 0x03, 0x00, 0x30, 0x51, 0x30) ) {
                break;  // print PDF417-code
            }

            break;
        }


        default:
            lastError = CU_PRINT_ERR__INVALID_BC_TYPE;

            break;
    }

    va_end( ap );

    if( CU_PRINT_ERR__NONE != lastError ) {
        return false;
    }

    if( ! sendCmdBuffer(10000) ) {
        clearCmdBuffer();

        return false;
    }

    return true;
}
//--------------------------------------------------------------------
//--------------------------------------------------------------------



///*******************************************************************
///PROTECTED METHODS OF OKI_THERMAL_PRINTER CLASS
///*******************************************************************


///*******************************************************************
///PRIVATE METHODS OF OKI_THERMAL_PRINTER CLASS
///*******************************************************************


const char*
TCU_OkiThermal::getVersionStr()
{
    return OKI_TP__VERSION_STRING;
}
//--------------------------------------------------------------------

bool
TCU_OkiThermal::beforePrintJobStart( void )
{
    lastError = CU_PRINT_ERR__NONE;

    if( SP_OK != sp_flush(SP, SP_BUF_INPUT) ) {
        lastError = CU_PRINT_ERR__PORT_FLUSH_DATA;
        return false;
    }

    return true;
}
//--------------------------------------------------------------------

bool
TCU_OkiThermal::finalizePrintJob( json_object* jPJob )
{
    char    idStr[8];
    char    wnd[2] = { 0, 0 };
    uint8_t asb[4], offs = 0;


    snprintf( idStr, sizeof(idStr), "%.4X", 0xffff & rand() );

    if( 11 == fillCmdBuffer(11, CH__GS, '(', 'H', 6, 0, 48, 48, idStr[0], idStr[1], idStr[2], idStr[3]) ) {
        if( ! sendCmdBuffer(1000) ) {
            return false;
        }
    }
    else return false;

    int pjProgress = json_object_get_int( json_object_object_get(jPJob, "pjProgress") );
    int ioResult;


    do {
        if( (ioResult = sp_input_waiting(SP)) < 0 ) {
            lastError = CU_PRINT_ERR__PORT_READ;

            break;
        }
        if( 0 == ioResult ) continue;

        if( (ioResult = sp_nonblocking_read(SP, wnd, 1)) < 0 ) {
            lastError = CU_PRINT_ERR__PORT_READ;

            break;
        }

        if( ioResult ) {
            if( 0x3722 == (*(uint16_t*) wnd) ) {
                char    buf[8];


                offs = 0;
                ioResult = sp_blocking_read( SP, buf, 5, 1000 );

                if( ioResult < 5 ) {
                    lastError = ( ioResult < 0 )? CU_PRINT_ERR__PORT_READ: CU_PRINT_ERR__PORT_IO_TIMEOUT;
                }

                if( strncmp(idStr, buf, 4) ) {
                    pjProgress = atoi( buf );
                }
                else break;
            }
            else {
                wnd[1] = wnd[0];

                if( offs < sizeof(asb) ) {
                    asb[offs++] = wnd[0];
                }

                if( sizeof(asb) == offs ) {
                    if( (0x10 != (asb[0] & 0x93)) || (0x00 != (asb[1] & 0x90)) || \
                        (0x00 != (asb[2] & 0xF0)) || (0x0F != (asb[3] & 0x9F)) ) {
                        lastError = CU_PRINT_ERR__INVALID_ASB_DATA;

                        break;
                    }

                    offs = 0;

                    if( asb[0] & 0x08 ) {
                    // offline state
                        if( ! this->cancelPrintJob() ) {
                            break;
                        }

                        if( asb[0] & 0x20 ) {
                            lastError = CU_PRINT_ERR__COVER_IS_OPENED;
                        }
                        else
                        if( asb[2] & 0x0C ) {
                            lastError = CU_PRINT_ERR__PAPER_END;
                        }
                        else lastError = CU_PRINT_ERR__OFFLINE_STATE;

                        break;
                    }
                }
            }
        }

        usleep( 10000 );
    }
    while( true );

    json_object_object_del( jPJob, "pjProgress" );
    json_object_object_add( jPJob, "pjProgress", json_object_new_int(pjProgress) );


    return ( CU_PRINT_ERR__NONE == lastError );
}
//--------------------------------------------------------------------

bool
TCU_OkiThermal::updateState()
{
    uint8_t status[4];
    uint8_t mask0 = 0x81, mask1 = 0x12;
    char    buf[32];


    lastError = CU_PRINT_ERR__NONE;
    State.RawBytes = "";

    for( size_t  n = 0; n < sizeof(status); n++ ) {
        int result = getRealTimeStatusByte( n + 1 );

        if( result < 0 ) {
            return false;
        }
        else status[n] = (uint8_t) result;

//        sprintf( buf, "%.2hhX", status[n] );
//        State.RawBytes += std::string( buf, 2 );

        if( (mask0 != (~status[n] & mask0)) || (mask1 != (status[n] & mask1)) ) {
            lastError = CU_PRINT_ERR__INVALID_STATUS_DATA;

            return false;
        }
    }

    State.online                = ( status[0] & 0x08 )? false: true;
    State.coverIsOpened         = ( status[1] & 0x04 )? true: false;
    State.errorsDetected        = ( status[1] & 0x40 )? true: false;
    State.recoverableError      = ( status[2] & 0x40 )? true: false;
    State.unrecoverableError    = ( status[2] & 0x20 )? true: false;
    State.paperNearEndDetected  = ( status[3] & 0x0C )? true: false;
    State.paperEndDetected      = ( status[3] & 0x60 )? true: false;
    State.lowBatteryVoltage     = false;
    State.overheatDetected      = false;


    if( State.online ) {
        sprintf( buf, "%d", getCounterValue(20) );
        State.RawBytes = buf;
    }


    return true;
}
//--------------------------------------------------------------------

bool
TCU_OkiThermal::afterProcessLine( json_object* jLine, int idx )
{
    lastError = CU_PRINT_ERR__NONE;

    if( json_object_is_type(jLine, json_type_string) ) {
        char    buf[8];


        snprintf( buf, sizeof(buf), "%.4d", idx );

        if( 11 != fillCmdBuffer(11, CH__GS, '(', 'H', 6, 0, 48, 48, buf[0], buf[1], buf[2], buf[3]) ) {
            clearCmdBuffer();

            return false;
        }

        return sendCmdBuffer(1000);
    }

    return true;
}
//--------------------------------------------------------------------

bool
TCU_OkiThermal::putCmd_Print()
{
    return fillCmdBuffer( 1, '\n' );
}
//--------------------------------------------------------------------

bool
TCU_OkiThermal::putCmd_SetAlignment( TCU_PropertyVal propVal )
{
    int alProp;


    switch( propVal ) {
        case pv_Left:   alProp = 0; break;
        case pv_Center: alProp = 1; break;
        case pv_Right:  alProp = 2; break;

        default:
            lastError = CU_PRINT_ERR__INVALID_PROP_VAL;

            return false;
    }


    return fillCmdBuffer( 3, CH__ESC, 'a', alProp );
}
//--------------------------------------------------------------------
bool
TCU_OkiThermal::putCmd_SetBarcodeWidth( int widthCode )
{
    if( (widthCode < 1) || (widthCode > 6) ) {
        widthCode = 3;
    }

    return fillCmdBuffer( 3, CH__GS, 'w', widthCode );
}
//--------------------------------------------------------------------

bool
TCU_OkiThermal::putCmd_SetBarcodeHeight( int heightDots )
{
    if( (heightDots < 1) || (heightDots > 255) ) {
        heightDots = 162;
    }

    return fillCmdBuffer( 3, CH__GS, 'h', heightDots );
}
//--------------------------------------------------------------------

bool
TCU_OkiThermal::putCmd_SetBarcodeHriPos( TCU_PropertyVal propVal )
{
    int hriProp;


    switch( propVal ) {
        case pv_None:   hriProp = 0; break;
        case pv_Above:  hriProp = 1; break;
        case pv_Below:  hriProp = 2; break;
        case pv_Both:   hriProp = 3; break;

        default:
            lastError = CU_PRINT_ERR__INVALID_PROP_VAL;

            return false;
    }

    return fillCmdBuffer( 3, CH__GS, 'H', hriProp );
}

//--------------------------------------------------------------------

bool
TCU_OkiThermal::putCmd_SetCharacterSize( TCU_PropertyVal propVal )
{
    int chrSize;


    switch( propVal ) {
        case pv_StdSize:    chrSize = 0x00; break;
        case pv_DblSize:    chrSize = 0x11; break;
        case pv_DblHeight:  chrSize = 0x01; break;
        case pv_DblWidth:   chrSize = 0x10; break;

        default:
            lastError = CU_PRINT_ERR__INVALID_PROP_VAL;

            return false;
    }

    return fillCmdBuffer( 3, CH__GS, '!', chrSize );
}
//--------------------------------------------------------------------

int
TCU_OkiThermal::getRealTimeStatusByte( unsigned char n )
{
    lastError = CU_PRINT_ERR__NONE;

    if( (n < 1) || (n > 4) ) {
        lastError = CU_PRINT_ERR__ARG_IS_OUT_OF_RANGE;
        return (-1);
    }

    char    txBuf[] = { CH__DLE, CH__EOT, n };
    char    rxByte = 0;
    int     ioResult;


    if( SP_OK != sp_flush(SP, SP_BUF_INPUT) ) {
        lastError = CU_PRINT_ERR__PORT_FLUSH_DATA;
        return (-1);
    }

    ioResult = sp_blocking_write( SP, txBuf, sizeof(txBuf), 1000 );
    if( ioResult < (int) sizeof(txBuf) ) {
        lastError = ( ioResult < 0 )? CU_PRINT_ERR__PORT_WRITE: CU_PRINT_ERR__PORT_IO_TIMEOUT;
        return (-1);
    }

    ioResult = sp_blocking_read( SP, &rxByte, 1, 2000 );
    if( ioResult < 1 ) {
        lastError = ( ioResult < 0 )? CU_PRINT_ERR__PORT_READ: CU_PRINT_ERR__PORT_IO_TIMEOUT;
        return (-1);
    }


    return rxByte;
}
//--------------------------------------------------------------------

int
TCU_OkiThermal::getCounterValue( int counterId )
{
    char    txBuf[] = { CH__GS, 'g', '2', 0, (char) (counterId % 256), 0 };
    int     ioResult;


    lastError = CU_PRINT_ERR__NONE;

    if( SP_OK != sp_flush(SP, SP_BUF_INPUT) ) {
        lastError = CU_PRINT_ERR__PORT_FLUSH_DATA;
        return (-1);
    }

    ioResult = sp_blocking_write( SP, txBuf, sizeof(txBuf), 1000 );
    if( ioResult < (int) sizeof(txBuf) ) {
        lastError = ( ioResult < 0 )? CU_PRINT_ERR__PORT_WRITE: CU_PRINT_ERR__PORT_IO_TIMEOUT;
        return (-1);
    }

    char    rxBuf[64];
    size_t  offs = 0;


    do {
        ioResult = sp_blocking_read( SP, &rxBuf[offs], 1, 2000 );
        if( ioResult < 1 ) {
            lastError = ( ioResult < 0 )? CU_PRINT_ERR__PORT_READ: CU_PRINT_ERR__PORT_IO_TIMEOUT;

            return (-1);
        }

        if( '\0' == rxBuf[offs] ) {
            return atoi( &rxBuf[1] );
        }

        if( '_' == rxBuf[0] ) {
            offs++;
        }
    }
    while( offs < sizeof(rxBuf) );

    lastError = CU_PRINT_ERR__BUFFER_SPACE;

    return (-1);
}

//--------------------------------------------------------------------
char*
TCU_OkiThermal::allocBuffer( const char* str )
{
    if( NULL == str ) {
        return NULL;
    }

    char*   buf = (char*) malloc( strlen(str) + 1 );

    if( NULL != buf ) {
        strcpy( buf, str );

        size_t  i = 0, n;

        while( (n = strlen(&buf[i])) >= 2) {
            if( (buf[i] == 0xD0) && (buf[i+1] == 0x86) ) {
                buf[i] = 'I';
                memmove( &buf[i+1], &buf[i+2], n - 1 );
            }
            else
            if( (buf[i] == 0xD1) && (buf[i+1] == 0x96) ) {
                buf[i] = 'i';
                memmove( &buf[i+1], &buf[i+2], n - 1 );
            }
            else    // New substitutions for diacritics
            if( (buf[i] == 0xC4) && (buf[i+1] == 0x86) ) {
                buf[i] = 0x5C;
                memmove( &buf[i+1], &buf[i+2], n - 1 );
            }
            else
            if( (buf[i] == 0xC4) && (buf[i+1] == 0x8C) ) {
                buf[i] = '{';
                memmove( &buf[i+1], &buf[i+2], n - 1 );
            }
            else
            if( (buf[i] == 0xC5) && (buf[i+1] == 0x81) ) {
                buf[i] = '$';
                memmove( &buf[i+1], &buf[i+2], n - 1 );
            }
            else
            if( (buf[i] == 0xC4) && (buf[i+1] == 0xB9) ) {
                buf[i] = '`';
                memmove( &buf[i+1], &buf[i+2], n - 1 );
            }
            else
            if( (buf[i] == 0xC5) && (buf[i+1] == 0x83) ) {
                buf[i] = '&';
                memmove( &buf[i+1], &buf[i+2], n - 1 );
            }
            else
            if( (buf[i] == 0xC5) && (buf[i+1] == 0x9A) ) {
                buf[i] = '^';
                memmove( &buf[i+1], &buf[i+2], n - 1 );
            }
            else
            if( (buf[i] == 0xC5) && (buf[i+1] == 0xA0) ) {
                buf[i] = '}';
                memmove( &buf[i+1], &buf[i+2], n - 1 );
            }
            else
            if( (buf[i] == 0xC5) && (buf[i+1] == 0xAC) ) {
                buf[i] = '|';
                memmove( &buf[i+1], &buf[i+2], n - 1 );
            }
            else
            if( (buf[i] == 0xC5) && (buf[i+1] == 0xB9) ) {
                buf[i] = '~';
                memmove( &buf[i+1], &buf[i+2], n - 1 );
            }
            else
            if( (buf[i] == 0xC5) && (buf[i+1] == 0xBD) ) {
                buf[i] = '@';
                memmove( &buf[i+1], &buf[i+2], n - 1 );
            }
            else i++;

        }
    }

    return buf;
}
//--------------------------------------------------------------------
