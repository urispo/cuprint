#include "cu_print.h"
#include "utils.h"

#define NEXGO_N86__VERSION_STRING  "0.0"

///*******************************************************************
///PUBLIC METHODS OF NEXGO_N86 CLASS
///*******************************************************************

TCU_NexgoN86::TCU_NexgoN86( const char* file, const char* charset ) :
TCU_BasePrinter( NULL )
{
    if( NULL != charset ) {
        if( ((iconv_t)(-1)) == (hconv = iconv_open(charset, ifceCS)) ) {
            lastError = CU_PRINT_ERR__CREATE_ENC_HANDLE;

            return;
        }
    }

    modelId = CU_PRINT_MODEL_ID__NEXGON86;

    Device.open( file, std::fstream::in | std::fstream::out | std::fstream::trunc );

    if( Device.fail() ) {
        lastError = CU_PRINT_ERR__PORT_OPEN;
    }
    //else DevFileName = file;

    Device >> DevFileName; 
    Device.close();
}
//--------------------------------------------------------------------

TCU_NexgoN86::~TCU_NexgoN86()
{
    // if( Device.is_open() ) {
    //     Device.close();
    // }
}
//--------------------------------------------------------------------

bool
TCU_NexgoN86::init( json_object* jSettings )
{
    lastError = CU_PRINT_ERR__NONE;

    pAlignment = pv_Left;

    // if( ! Device.is_open() ) {
    //     lastError = CU_PRINT_ERR__PORT_OPEN;

    //     return false;
    // }

    return true;
}
//--------------------------------------------------------------------

bool
TCU_NexgoN86::cancelPrintJob()
{
    lastError = CU_PRINT_ERR__NONE;

    return true;
}
//--------------------------------------------------------------------

bool
TCU_NexgoN86::printText( const char* text )
{
    json_object_array_add(jLinesArray, json_object_new_string(text));
    return true;
}
//--------------------------------------------------------------------

bool
TCU_NexgoN86::sendRawDataBin( uint8_t* buffer, size_t bytesCount )
{
    lastError = CU_PRINT_ERR__UNSUPPORTED_FEATURE;
    return false;
}
//--------------------------------------------------------------------

bool
TCU_NexgoN86::feedPaper( TCU_FeedMode fpMode, int units )
{
    json_object_array_add(jLinesArray, json_object_new_int(60000)); // + units
    return true;
}
//--------------------------------------------------------------------
bool
TCU_NexgoN86::cutPaper( TCU_CutterType coType, int adv )
{
    lastError = CU_PRINT_ERR__NONE;
    return true;
}
//--------------------------------------------------------------------

bool
TCU_NexgoN86::printBitImage( int mode )
{
    json_object_array_add(jLinesArray, json_object_new_int(10000));
    return true;
}
//--------------------------------------------------------------------

bool
TCU_NexgoN86::printBarCode( TCU_BarcodeType bcType, const char* bcData, ... )
{
    va_list ap;
    int     codeLen = strlen( bcData );
    va_start( ap, bcData );

    switch( bcType ) {
        case bcType_Code128:
            json_object_array_add(jLinesArray, json_object_new_int(30000));
            json_object_array_add(jLinesArray, json_object_new_string(bcData));
            break;

        case bcType_QR: {
            int     qrSize = va_arg( ap, int );
            int     qrEccl = va_arg( ap, int );


            if( (qrSize < 1) || (qrSize > 14) || (qrEccl < 1) || (qrEccl > 4) ) {
                lastError = CU_PRINT_ERR__INVALID_PROP_VAL;

                break;
            }
            if( codeLen > 448 ) {
                lastError = CU_PRINT_ERR__INVALID_PROP_VAL;

                break;
            }

            if( codeLen <= 0 ) {
                lastError = CU_PRINT_ERR__INVALID_PROP_VAL;

                break;
            }

            if (qrSize < 7) 
                json_object_array_add(jLinesArray, json_object_new_int(20001));
            else 
                json_object_array_add(jLinesArray, json_object_new_int(20000));
            json_object_array_add(jLinesArray, json_object_new_string(bcData));

            break;
        }

        case bcType_PDF417: {
            int     pdfModuleWidth = va_arg( ap, int ); //
            int     pdfHeight = va_arg( ap, int );
            int     pdfEccl = va_arg( ap, int );    // 1..9 (9 - auto)

            int     pdfType = (pdfModuleWidth >> 8) & 0x0F; // 0 - standard, 1 - truncated
            int     pdfEncMode = pdfModuleWidth & 0x0F; // 0 - automatic, 1 - binary

            if (   (pdfType < 0) || (pdfType > 1) \
                || (pdfEncMode < 0) || (pdfEncMode > 1) \
                || (pdfEccl < 0) || (pdfEccl > 9) \
                || (pdfHeight < 0) || (pdfHeight > 15) ) {
                lastError = CU_PRINT_ERR__INVALID_PROP_VAL;

                break;
            }
            if( codeLen > 384 ) {
                lastError = CU_PRINT_ERR__INVALID_PROP_VAL;

                break;
            }

            json_object_array_add(jLinesArray, json_object_new_int(40000));
            json_object_array_add(jLinesArray, json_object_new_string(bcData));

            break;
        }

        default:
            lastError = CU_PRINT_ERR__INVALID_BC_TYPE;
            break;
    }

    va_end( ap );    

    return true;
}
//--------------------------------------------------------------------
//--------------------------------------------------------------------
//--------------------------------------------------------------------


///*******************************************************************
///PROTECTED METHODS OF VIRTUAL_PRINTER CLASS
///*******************************************************************
//--------------------------------------------------------------------
//--------------------------------------------------------------------
//--------------------------------------------------------------------



///*******************************************************************
///PRIVATE METHODS OF VIRTUAL_PRINTER CLASS
///*******************************************************************

bool
TCU_NexgoN86::beforePrintJobStart()
{
    // if( Device.is_open() ) {
    //     Device.close();
    // }
    // Device.open( DevFileName.c_str(), std::fstream::in | std::fstream::out | std::fstream::trunc );

    // lastError = CU_PRINT_ERR__NONE;

    // if( Device.fail() ) {
    //     lastError = CU_PRINT_ERR__PORT_OPEN;

    //     return false;
    // }

    sock = socket(AF_INET, SOCK_STREAM, 0);
    if (sock < 0)
    {
        return false;
    }

    addr.sin_family = AF_INET;
    addr.sin_port = htons(NEXGO_PRINTER_PORT);
    addr.sin_addr.s_addr = inet_addr(DevFileName);
    if (connect(sock, (struct sockaddr *)&addr, sizeof(addr)) < 0)
    {
        return false;
    }

    // jPrintData = json_object_new_object();
    jLinesArray = json_object_new_array();
    // json_object_object_add(jPrintData, "lines", jLinesArray);
    
    return true;
}
//--------------------------------------------------------------------

bool
TCU_NexgoN86::afterPrintJobFinish( bool result )
{
    // if( result ) {
    //     Device.close();
    // }

    close(sock);
    
    if (jLinesArray)
        json_object_put(jLinesArray);
    if (jPrintData)
        json_object_put(jPrintData);
    jLinesArray = NULL;
    jPrintData = NULL;

    return result;
}
//--------------------------------------------------------------------

const char*
TCU_NexgoN86::getVersionStr()
{
    return PS_VIRTUAL__VERSION_STRING;
}
//--------------------------------------------------------------------

bool
TCU_NexgoN86::updateState()
{
    lastError = CU_PRINT_ERR__NONE;

    jPrintData = json_object_new_object();
    json_object_object_add(jPrintData, "lines", jLinesArray);

    std::string resp = json_object_to_json_string_ext(jPrintData, 0) + "\n";

    char* text = resp.c_str();
    ssize_t     alreadySent = 0;
    ssize_t     leftToSend = strlen( text );

    while( alreadySent < leftToSend ) {
        ssize_t sent = send( sock, text + alreadySent, leftToSend, 0 );

        if( sent < 0 ) {
            lastError = CU_PRINT_ERR__PORT_SEND_DATA;
            return false;
        }

        alreadySent += sent;
        leftToSend  -= sent;
    }    

    char buf[256];
    int nread = recv(sock, buf, sizeof(buf), 0);
    if (nread <= 0) {
        lastError = CU_PRINT_ERR__PORT_READ;
        return true;
    }
    buf[nread] = '\0';
    int prRes = atoi(buf);

    State.online = true;
    State.errorsDetected = false;
    State.unrecoverableError = false;
    State.recoverableError = false;
    State.paperEndDetected = false;
    State.paperNearEndDetected = false;
    State.coverIsOpened = false;

    if (prRes = 0)
        State.online = true;
    else if (prRes == -1005) {
        State.online = false;
        State.paperEndDetected = true;
    }
    else {
        State.online = false;
        State.errorsDetected = true;
    }

    return true;
}
//--------------------------------------------------------------------

bool
TCU_NexgoN86::finalizePrintJob( json_object* jPJob )
{
    jPrintData = json_object_new_object();
    json_object* jItem = json_object_new_int(1);
    json_object_object_add(jPrintData, "status", jItem);
    std::string resp = json_object_to_json_string_ext(jPrintData, 0) + "\n";

    char* text = resp.c_str();
    ssize_t     alreadySent = 0;
    ssize_t     leftToSend = strlen( text );

    while( alreadySent < leftToSend ) {
        ssize_t sent = send( sock, text + alreadySent, leftToSend, 0 );

        if( sent < 0 ) {
            lastError = CU_PRINT_ERR__PORT_SEND_DATA;
            return false;
        }

        alreadySent += sent;
        leftToSend  -= sent;
    }    

    char buf[256];
    int nread = recv(sock, buf, sizeof(buf), 0);
    if (nread <= 0) {
        lastError = CU_PRINT_ERR__PORT_READ;
        return true;
    }
    buf[nread] = '\0';
    int prRes = atoi(buf);
    if (prRes = 0)
        lastError = CU_PRINT_ERR__NONE;
    else if (prRes == -1005)
        lastError = CU_PRINT_ERR__PAPER_END;
    else lastError = CU_PRINT_ERR__ENC_UNKNOWN_CRASH;

    json_object_put(jItem);
    json_object_put(jPrintData);
    jPrintData = NULL;
    jItem = NULL;
    return true;
}
//--------------------------------------------------------------------

bool
TCU_NexgoN86::putCmd_Print()
{
    Device << std::endl;

    return Device.good();
}
//--------------------------------------------------------------------

bool
TCU_NexgoN86::putCmd_SetAlignment( TCU_PropertyVal propVal )
{
    lastError = CU_PRINT_ERR__NONE;

    switch( propVal ) {
        case pv_Left:
            json_object_array_add(jLinesArray, json_object_new_int(0));
            break;
        case pv_Center:
            json_object_array_add(jLinesArray, json_object_new_int(1));
            break;        
        case pv_Right:
            json_object_array_add(jLinesArray, json_object_new_int(2));
            break;

        default: lastError = CU_PRINT_ERR__INVALID_PROP_VAL; break;
    }

    return ( CU_PRINT_ERR__NONE == lastError );
}
//--------------------------------------------------------------------

bool
TCU_NexgoN86::putCmd_SetBarcodeWidth( int widthCode )
{
    lastError = CU_PRINT_ERR__NONE;

    return true;
}
//--------------------------------------------------------------------

bool
TCU_NexgoN86::putCmd_SetBarcodeHeight( int heightDots )
{
    lastError = CU_PRINT_ERR__NONE;

    return true;
}
//--------------------------------------------------------------------

bool
TCU_NexgoN86::putCmd_SetBarcodeHriPos( TCU_PropertyVal propVal )
{
    lastError = CU_PRINT_ERR__NONE;

    return true;
}
//--------------------------------------------------------------------
bool
TCU_NexgoN86::putCmd_SetCharacterSize( TCU_PropertyVal propVal )
{
    int chrSize;

    switch( propVal ) {
        case pv_StdSize:    
            json_object_array_add(jLinesArray, json_object_new_int(3));
            break;
        case pv_DblHeight:    
            json_object_array_add(jLinesArray, json_object_new_int(4));
            break;
        case pv_DblWidth:
        case pv_DblSize:    
            break;

        default:
            lastError = CU_PRINT_ERR__INVALID_PROP_VAL;

            return false;
    }

    return true;
}
//--------------------------------------------------------------------
